import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/view_models/chat_screen_view_model.dart';
import 'package:flutter_messenger/widgets/list_items/message_item_widget.dart';
import 'package:flutter_messenger/widgets/loading_indicator_widget.dart';
import 'package:flutter_messenger/widgets/text_input_widget.dart';
import 'package:flutter_redux/flutter_redux.dart';

/// Chat details screen with list of chat messages and input field

@immutable
class ChatScreen extends StatefulWidget {
  ChatScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final ScrollHolder _holder = new ScrollHolder();
  final ScrollController _scrollController = ScrollController();

  _ChatScreenState();

  @override
  Widget build(BuildContext context) => StoreConnector<AppState, ChatScreenViewModel>(
        distinct: true,
        converter: (store) => createChatScreenViewModel(store, context),
        builder: (context, vm) => Scaffold(
              appBar: AppBar(title: Text(vm.chatTheme)),
              body: Stack(
                children: _buildBody(context, vm),
              ),
              floatingActionButton: _buildFAB(context),
            ),
      );

  /// Screen consists of List of Widgets in Stack
  List<Widget> _buildBody(BuildContext context, ChatScreenViewModel vm) {
    if (!_holder.isListLocked) {
      _holder.displayedCount = vm.messagesCount;
    }
    // actual messages count is always updated
    _holder.actualCount = vm.messagesCount;

    var stackList = List<Widget>();
    stackList.add(Column(
      children: [
        Expanded(
          child: _holder.displayedCount == 0 ? Center(child: Text(emptyMessagesListText)) : _buildMessagesList(vm),
        ),
        Divider(
          height: 1.0,
          color: Theme.of(context).accentColor,
        ),
        Container(
          decoration: BoxDecoration(color: Theme.of(context).cardColor.withOpacity(0.1)),
          child: TextInputWidget(
            (String message) {
              _scrollToBottom(duration: 1);
              vm.onSubmitMessage(message);
            },
            vm.onOpenMedia,
          ),
        ),
      ],
    ));

    // progress if needed
    if (vm.isLoading) {
      stackList.add(LoadingIndicatorWidget());
    }
    return stackList;
  }

  /// Building FAB widget or null
  Widget _buildFAB(BuildContext context) => _holder.unreadCount == 0
      ? null
      : Container(
          margin: EdgeInsets.only(bottom: 48.0),
          child: FloatingActionButton(
              mini: true,
              child: Stack(
                children: _buildFabContent(context),
              ),
              onPressed: () => _scrollToBottom()),
        );

  /// List of widgets for FAB
  List<Widget> _buildFabContent(BuildContext context) {
    var fabWidgets = List<Widget>();
    if (_holder.unreadCount != 0) {
      fabWidgets.add(
        Positioned(
          top: 0.0,
          right: 0.0,
          child: Container(
            alignment: AlignmentDirectional.center,
            padding: const EdgeInsets.all(0.5),
            decoration: BoxDecoration(
              color: Colors.redAccent,
              borderRadius: const BorderRadius.all(Radius.circular(3.0)),
            ),
            child: Text(
              _holder.unreadCount.toString(),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 8.0,
              ),
            ),
          ),
        ),
      );
    }

    fabWidgets.add(Icon(
      Icons.keyboard_arrow_down,
      size: 32.0,
      color: Colors.white,
    ));

    return fabWidgets;
  }

  Widget _buildMessagesList(ChatScreenViewModel viewModel) => NotificationListener(
        child: Scrollbar(
          child: ListView.builder(
            controller: _scrollController,
            padding: EdgeInsets.all(8.0),
            reverse: true,
            itemCount: _holder.displayedCount,
            itemBuilder: (_, int index) => MessageItemWidget(
                  viewModel.getMessageEntry(
                    count: _holder.displayedCount,
                    index: index,
                  ),
                  viewModel.onDeleteMessage,
                ),
          ),
        ),
        onNotification: (notification) {
          if (notification is ScrollNotification) {
            var position = _scrollController.position;

            /// save messages count while scrolling up and not locked yet
            if (!_holder.isListLocked &&
                (position.pixels > _holder.scrollOffset) &&
                (position.extentBefore > position.extentInside / 3)) {
              _holder.displayedCount = viewModel.messagesCount;
              _holder.isListLocked = true;

              /// unlock and update messages count
            } else if ((position.pixels < _holder.scrollOffset) &&
                (position.extentBefore < position.extentInside / 2)) {
              _holder.isListLocked = false;
              _holder.displayedCount = viewModel.messagesCount; // can be removed
              setState(() {});
            }
            _holder.scrollOffset = position.pixels;
          }
          return true;
        },
      );

  /// paste 1 for immediate scroll to bottom
  void _scrollToBottom({int duration = 300}) {
    if (_holder.scrollOffset > 0.0) {
      _scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: Duration(milliseconds: duration),
      );
    }
  }
}

/// Data holder for handling list lock while user scrolled list up

class ScrollHolder {
  double _scrollOffset = 0.0;
  int _displayedCount = 0;
  int _actualCount = 0;
  bool _isListLocked = false;

  int get displayedCount => _displayedCount;

  set displayedCount(int value) => _displayedCount = value;

  int get actualCount => _actualCount;

  set actualCount(int value) => _actualCount = value;

  bool get isListLocked => _isListLocked;

  set isListLocked(bool value) => _isListLocked = value;

  double get scrollOffset => _scrollOffset;

  set scrollOffset(double value) => _scrollOffset = value;

  get unreadCount => _actualCount - _displayedCount;
}
