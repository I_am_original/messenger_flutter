import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/view_models/chat_creation_view_model.dart';
import 'package:flutter_messenger/widgets/list_items/selected_user_item_widget.dart';
import 'package:flutter_messenger/widgets/list_items/user_item_widget.dart';
import 'package:flutter_messenger/widgets/screens/chat_creation_data_screen.dart';
import 'package:flutter_redux/flutter_redux.dart';

/// First page of ChatCreation Flow
/// Contains Users List for selection
class ChatCreationUsersScreen extends StatefulWidget {
  final String _theme;
  final String _description;
  final List<String> _selection;

  ChatCreationUsersScreen({
    String theme,
    String description,
    List<String> selection,
  })  : _theme = theme,
        _description = description,
        _selection = selection;

  @override
  ChatCreationUsersState createState() => ChatCreationUsersState();
}

class ChatCreationUsersState extends State<ChatCreationUsersScreen> {
  @override
  Widget build(BuildContext context) =>
      StoreConnector<AppState, ChatCreationViewModel>(
        rebuildOnChange: false,
        converter: (store) => ChatCreationViewModel.create(
            store, widget._selection == null ? [] : widget._selection),
        builder: (context, vm) => Scaffold(
              appBar: AppBar(
                leading: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  tooltip: closeApp,
                  onPressed: () => Navigator.pop(context),
                ),
                title: Text(selectUpToSixUsers),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      next,
                      style: TextStyle(
                          color: vm.isSelectionNotEmpty()
                              ? Theme.of(context).accentColor
                              : Theme.of(context).disabledColor),
                    ),
                    onPressed: vm.isSelectionNotEmpty()
                        ? () => Navigator.pushReplacement(
                              context,
                              CupertinoPageRoute(
                                builder: (context) => ChatCreationDataScreen(
                                      theme: widget._theme,
                                      description: widget._description,
                                      selection: vm.selectedUserIds,
                                    ),
                                fullscreenDialog: true,
                              ),
                            )
                        : null,
                  ),
                ],
              ),
              body: _buildBody(vm),
            ),
      );

  Widget _buildBody(ChatCreationViewModel vm) => Column(
        children: <Widget>[
          Expanded(
            flex: 0,
            child: Container(
              margin: EdgeInsets.symmetric(
                  vertical: vm.isSelectionNotEmpty() ? 10.0 : 0.0),
              height: vm.isSelectionNotEmpty() ? 100.0 : 0.0,
              child: Scrollbar(
                child: ListView.builder(
                  itemCount: vm.selectionCount(),
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemBuilder: (context, index) =>
                      SelectedUserWidget(vm.getSelectedUser(index)),
                ),
              ),
            ),
          ),
          Expanded(
            child: Scrollbar(
              child: ListView.builder(
                  itemCount: vm.usersCount,
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    final User user = vm.getUser(index);
                    return UserSelectionWidget(
                        user: user,
                        isSelected: vm.isUserSelected(user.id),
                        isActive: vm.isSelectionActive(user.id),
                        onChangeAction: (id, value) {
                          vm.changeUserSelection(
                              userId: id, addSelection: value);
                          _updateWidget();
                        });
                  }),
            ),
          ),
        ],
      );

  void _updateWidget() => setState(() {});
}
