import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/view_models/chat_creation_view_model.dart';
import 'package:flutter_messenger/widgets/list_items/selected_user_item_widget.dart';
import 'package:flutter_messenger/widgets/screens/chat_creation_users_screen.dart';
import 'package:flutter_redux/flutter_redux.dart';

/// Second page of ChatCreation Flow
/// Contains data fill fields for chat name and description
class ChatCreationDataScreen extends StatefulWidget {
  final String _theme;
  final String _description;
  final List<String> _selection;

  ChatCreationDataScreen({
    String theme,
    String description,
    List<String> selection,
  })  : _theme = theme,
        _description = description,
        _selection = selection;

  @override
  ChatCreationDataState createState() =>
      ChatCreationDataState(_theme, _description);
}

class ChatCreationDataState extends State<ChatCreationDataScreen> {
  // InputControllers serve as data sources
  final TextEditingController _themeInputController;
  final TextEditingController _descInputController;
  Timer _textChangeTimer;

  ChatCreationDataState(String theme, String description)
      : _themeInputController = TextEditingController(text: theme),
        _descInputController = TextEditingController(text: description);

  @override
  Widget build(BuildContext context) =>
      StoreConnector<AppState, ChatCreationViewModel>(
        rebuildOnChange: false,
        converter: (store) =>
            ChatCreationViewModel.create(store, widget._selection),
        builder: (context, vm) => WillPopScope(
              onWillPop: () => _buildBackAction(context, vm),
              child: Scaffold(
                appBar: AppBar(
                  leading: IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                    tooltip: closeApp,
                    onPressed: () => _buildBackAction(context, vm),
                  ),
                  title: Text(createNewChat),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(
                        done,
                        style: TextStyle(
                            color: _canCreateChat(vm)
                                ? Theme.of(context).accentColor
                                : Theme.of(context).disabledColor),
                      ),
                      onPressed: _canCreateChat(vm)
                          ? () => vm.onCreateAction(
                                context,
                                theme: _themeInputController.text.trim(),
                                description: _descInputController.text.trim(),
                                userIds: vm.selectedUserIds,
                              )
                          : null,
                    ),
                  ],
                ),
                body: _buildBody(vm),
              ),
            ),
      );

  Future _buildBackAction(BuildContext context, ChatCreationViewModel vm) =>
      Navigator.pushReplacement(
        context,
        CupertinoPageRoute(
          builder: (context) => ChatCreationUsersScreen(
                theme: _themeInputController.text.trim(),
                description: _descInputController.text.trim(),
                selection: vm.selectedUserIds,
              ),
          fullscreenDialog: true,
        ),
      );

  Widget _buildBody(ChatCreationViewModel vm) => Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
                vertical: vm.isSelectionNotEmpty() ? 10.0 : 0.0),
            height: vm.isSelectionNotEmpty() ? 100.0 : 0.0,
            child: Scrollbar(
              child: ListView.builder(
                itemCount: vm.selectionCount(),
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemBuilder: (context, index) =>
                    SelectedUserWidget(vm.getSelectedUser(index)),
              ),
            ),
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: TextField(
                    decoration: const InputDecoration(
                      labelText: newChatTheme,
                      filled: true,
                    ),
                    controller: _themeInputController,
                    onChanged: _listenInput,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: TextField(
                    decoration: const InputDecoration(
                      labelText: newChatDesc,
                      filled: true,
                    ),
                    controller: _descInputController,
                    onChanged: _listenInput,
                  ),
                ),
              ]),
        ],
      );

  void _updateWidget() => setState(() {});

  void _listenInput(String input) {
    _textChangeTimer?.cancel();
    _textChangeTimer = Timer(Duration(milliseconds: 50), () => _updateWidget());
  }

  bool _canCreateChat(ChatCreationViewModel vm) =>
      vm.canChangePhase(_themeInputController.text, _descInputController.text);
}
