import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/view_models/chat_widget_view_model.dart';
import 'package:flutter_messenger/view_models/home_screen_view_model.dart';
import 'package:flutter_messenger/widgets/list_items/chat_item_widget.dart';
import 'package:flutter_messenger/widgets/loading_indicator_widget.dart';
import 'package:flutter_messenger/widgets/locker_widget.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:dynamic_theme/dynamic_theme.dart';

/// This widget is the HomePage of application.
/// StoreConnector convert State in provided ViewModel for connecting with Store
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => StoreConnector<AppState, HomeScreenViewModel>(
        distinct: true,
        converter: (store) => createHomeViewModel(store),
        builder: (context, vm) => Scaffold(
              appBar: AppBar(
                title: Text(vm.appBarTitle),
                actions: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                    tooltip: closeApp,
                    onPressed: () => exit(0),
                  ),
                ],
              ),
              body: Stack(
                children: _buildBody(context, vm),
              ),
              drawer: Drawer(
                child: ListView(
                  // Important: Remove any padding from the ListView.
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    DrawerHeader(
                      child: Text(
                        settings,
                        style: Theme.of(context).accentTextTheme.display1,
                      ),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                      ),
                    ),

                    /// Lock switch
                    SwitchListTile.adaptive(
                      title: Text(
                        settingsLock,
                        style: Theme.of(context).textTheme.title,
                      ),
                      subtitle: Text(
                        vm.isLockEnabled ? settingsLockEnabled : settingsLockDisabled,
                        style: Theme.of(context).textTheme.subhead,
                      ),
                      activeColor: Theme.of(context).toggleableActiveColor,
                      value: vm.isLockEnabled,
                      onChanged: (isEnabled) => vm.onLockSettingsChanged(isEnabled),
                    ),

                    /// Theme switch
                    SwitchListTile.adaptive(
                      title: Text(
                        settingsTheme,
                        style: Theme.of(context).textTheme.title,
                      ),
                      value: Theme.of(context).brightness == Brightness.dark,
                      activeColor: Theme.of(context).toggleableActiveColor,
                      onChanged: (isEnabled) => vm.onThemeFlagChange(isEnabled, context),
                    ),
                  ],
                ),
              ),
              floatingActionButton: FloatingActionButton(
                backgroundColor: Theme.of(context).accentColor,
                onPressed: () => vm.onAddClicked(context),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
      );

  /// Screen consists of List of Widgets in Stack
  List<Widget> _buildBody(BuildContext context, HomeScreenViewModel vm) {
    var list = List<Widget>();
    // screen main body
    list.add(
      Center(
        child: vm.chatListSize == 0 ? Text(emptyChatListText) : _buildChatsList(vm),
      ),
    );

    // loading indicator if needed
    if (vm.isLoading) {
      list.add(LoadingIndicatorWidget());
    }

    // lock screen handling widget
    list.add(LockerWidget());

    return list;
  }

  /// Building ListView widget with builder method for each entry
  Widget _buildChatsList(HomeScreenViewModel viewModel) => Scrollbar(
        child: ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemCount: viewModel.chatListSize,
          itemBuilder: (context, index) => StoreConnector<AppState, ChatWidgetViewModel>(
                distinct: true,
                converter: (store) => createChatWidgetViewModel(
                      store: store,
                      index: index,
                    ),
                builder: (context, viewModel) => ChatItemWidget(viewModel),
              ),
        ),
      );
}
