import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/view_models/lock_screen_view_model.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

/// Lock screen with black background disable back button

class LockScreen extends StatefulWidget {
  static final LockScreen _instance = new LockScreen._internal();

  factory LockScreen() {
    return _instance;
  }

  LockScreen._internal();

  @override
  State<StatefulWidget> createState() => LockScreenState();
}

class LockScreenState extends State<LockScreen> {
  @override
  Widget build(BuildContext context) => StoreConnector<AppState, LockScreenViewModel>(
        distinct: true,
        rebuildOnChange: false,
        converter: (Store store) => createLockViewModel(store),
        builder: (BuildContext context, LockScreenViewModel vm) => WillPopScope(
              onWillPop: () => Future.value(false),
              child: Stack(
                children: [
                  BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                    child: Container(
                      decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    padding: const EdgeInsets.all(25.0),
                    child: FlatButton(
                      child: Text(
                        unlock,
                        style: Theme.of(context).textTheme.title,
                      ),
                      onPressed: () => vm.onUnlockClicked(context),
                    ),
                  ),
                ],
              ),
            ),
      );
}
