import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/view_models/chat_widget_view_model.dart';

/// View representation for each [Chat] from list

class ChatItemWidget extends StatelessWidget {
  final ChatWidgetViewModel viewModel;

  ChatItemWidget(this.viewModel);

  @override
  Widget build(BuildContext context) => Card(
        color: Theme.of(context).cardColor.withOpacity(0.5),
        elevation: 0,
        margin: const EdgeInsets.symmetric(vertical: 5.0),
        child: InkWell(
          onTap: () => viewModel.onChatClicked(context),
          child: Container(
            color: Colors.transparent,
            padding: const EdgeInsets.all(5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: Container(
                    margin: const EdgeInsets.all(5.0),
                    child: CircleAvatar(
                      child: Text(
                        viewModel.chatTheme[0],
                        style: Theme.of(context).textTheme.title,
                      ),
                      backgroundColor: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        viewModel.chatTheme,
                        style: Theme.of(context).textTheme.headline,
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: Text(viewModel.userName),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: Text(viewModel.lastMessageText),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
