import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/user.dart';

/// Widget represent User item in a list with checkbox for selection
/// Used for Chat Creation feature

class SelectedUserWidget extends StatelessWidget {
  final User user;

  SelectedUserWidget(this.user);

  @override
  Widget build(BuildContext context) => Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(16.0),
            child: CircleAvatar(
              child: Text(
                user.abbreviation,
                style: Theme.of(context).textTheme.title,
              ),
              backgroundColor: Theme.of(context).accentColor,
            ),
          ),
          Text(user.fullName),
        ],
      );
}
