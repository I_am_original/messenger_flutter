import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/functions.dart';
import 'package:flutter_messenger/resources/strings.dart';

class MessageItemWidget extends StatelessWidget {
  final MessageEntry messageEntry;
  final onDeleteMessage;

  MessageItemWidget(this.messageEntry, this.onDeleteMessage);

  @override
  Widget build(BuildContext context) {
    var isMe = messageEntry.isMine;
    final bg = isMe ? Theme.of(context).cardColor : Theme.of(context).accentColor;
    final textColor = Theme.of(context).textTheme.title.color;
    final icon = Icons.done;
    final messageBoxCorners = BorderRadius.only(
      topLeft: Radius.circular(isMe ? 10.0 : 0.0),
      topRight: Radius.circular(isMe ? 0.0 : 10.0),
      bottomLeft: Radius.circular(isMe ? 15.0 : 10.0),
      bottomRight: Radius.circular(isMe ? 10.0 : 15.0),
    );
    final spacing = EdgeInsets.only(
      left: isMe ? 50.0 : 3.0,
      right: isMe ? 3.0 : 50.0,
      top: 3.0,
      bottom: 3.0,
    );
    return InkWell(
      onLongPress: () {
        if (messageEntry.isMine) {
          _showDeleteDialog(context, messageEntry);
        }
      },
      child: Container(
        margin: spacing,
        padding: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                blurRadius: .5,
                spreadRadius: 1.0,
                color: Theme.of(context).accentColor.withOpacity(.12))
          ],
          color: bg.withOpacity(0.5),
          borderRadius: messageBoxCorners,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              messageEntry.authorName,
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontStyle: FontStyle.italic,
                color: textColor,
                fontSize: 14.0,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5.0, bottom: 5.0),
              child: Text(
                messageEntry.messageContent,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: textColor,
                  fontSize: 14.0,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  messageEntry.dateTime,
                  style: TextStyle(
                    color: textColor,
                    fontSize: 12.0,
                  ),
                ),
                SizedBox(width: 3.0),
                Icon(
                  icon,
                  size: 12.0,
                  color: textColor,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  /// Delete message dialog

  void _showDeleteDialog(BuildContext context, MessageEntry messageEntry) {
    showOwnDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: Text(dialogTitle),
            content: Text(dialogMessage(messageEntry.authorName)),
            actions: [
              FlatButton(
                  child: Text(cancel),
                  onPressed: () => Navigator.of(context).pop()),
              FlatButton(
                  child: Text(confirm),
                  onPressed: () {
                    onDeleteMessage(messageEntry.id);
                    Navigator.of(context).pop();
                  }),
            ],
          ),
    );
  }
}
