import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/user.dart';

/// Widget represent User item in a list with checkbox for selection
/// Used for Chat Creation feature

class UserSelectionWidget extends StatelessWidget {
  final User user;
  final bool isSelected;
  final bool isActive;
  final onChangeAction;

  UserSelectionWidget({
    @required this.user,
    @required this.isSelected,
    @required this.isActive,
    @required this.onChangeAction,
  });

  @override
  Widget build(BuildContext context) => CheckboxListTile(
        title: Text(user.fullName),
        onChanged: isActive
            ? (bool isChecked) => onChangeAction(user.id, isChecked)
            : null,
        value: isSelected,
        secondary: Container(
          margin: const EdgeInsets.only(right: 2.0),
          child: CircleAvatar(
            child: Text(
              user.abbreviation,
              style: Theme.of(context).textTheme.title,
            ),
            backgroundColor: isActive ? Theme.of(context).accentColor : Theme.of(context).disabledColor,
          ),
        ),
      );
}
