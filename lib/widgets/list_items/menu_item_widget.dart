import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/resources/strings.dart';

class MenuItem extends StatelessWidget {
  const MenuItem({Key key, this.itemData}) : super(key: key);

  final ItemData itemData;

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 12.0),
            child: Icon(itemData.icon, size: 24.0, color: Colors.blue),
          ),
          Text(itemData.title, style: Theme.of(context).textTheme.title),
        ],
      );
}

class ItemData {
  const ItemData({this.title, this.icon, @required this.route});

  final String title;
  final IconData icon;
  final String route;
}

const List<ItemData> mediaMenuItems = const <ItemData>[
  const ItemData(
    title: camera,
    icon: Icons.camera_alt,
    route: routeCameraScreen,
  ),
  const ItemData(
    title: gallery,
    icon: Icons.image,
    route: routeGalleryScreen,
  ),
];
