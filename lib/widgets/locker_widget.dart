import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/view_models/locker_view_model.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

/// Transparent Layer handles Lock events listening via ViewModel

class LockerWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LockerState();
}

class LockerState extends State<LockerWidget> {
  @override
  Widget build(BuildContext context) =>
      StoreConnector<AppState, LockerViewModel>(
        converter: (Store store) => createLockerViewModel(store, context),
        builder: (BuildContext context, LockerViewModel vm) =>
            Container(height: 0.0),
      );
}
