import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/widgets/list_items/menu_item_widget.dart';

@immutable
class TextInputWidget extends StatefulWidget {
  final _submitAction;
  final _navigateAction;

  TextInputWidget(this._submitAction, this._navigateAction);

  @override
  State<StatefulWidget> createState() =>
      _TextInputWidgetState(_submitAction, _navigateAction);
}

class _TextInputWidgetState extends State<TextInputWidget> {
  final TextEditingController _inputController = TextEditingController();
  final _submitAction;
  final _navigateAction;

  _TextInputWidgetState(this._submitAction, this._navigateAction);

  @override
  Widget build(BuildContext context) => IconTheme(
        data: IconThemeData(color: Theme.of(context).accentColor),
        child: Container(
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: PopupMenuButton<ItemData>(
                  onSelected: (menuItem) => _navigateAction(menuItem.route),
                  itemBuilder: (BuildContext context) {
                    return mediaMenuItems.map((ItemData data) {
                      return PopupMenuItem<ItemData>(
                        value: data,
                        child: MenuItem(itemData: data),
                      );
                    }).toList();
                  },
                ),
              ),
              Flexible(
                child: TextField(
                  controller: _inputController,
                  onSubmitted: _handleSubmission,
                  decoration: InputDecoration.collapsed(hintText: inputHint),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: IconButton(
                    icon: Icon(Icons.send),
                    onPressed: () => _handleSubmission(_inputController.text)),
              ),
            ],
          ),
        ),
      );

  void _handleSubmission(String text) {
    if (text.isNotEmpty) {
      _submitAction(text);
      _inputController.clear();
    }
  }
}
