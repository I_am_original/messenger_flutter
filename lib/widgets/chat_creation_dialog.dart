import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/view_models/chat_creation_view_model.dart';
import 'package:flutter_messenger/widgets/custom_alert_dialog.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:flutter_messenger/widgets/list_items/user_item_widget.dart';

/// Show dialog in two phases
/// First - General chat info filling
/// Second - Users selection for Chat

class ChatCreationDialog extends StatefulWidget {
  ChatCreationDialog({Key key}) : super(key: key);

  @override
  ChatCreationDialogState createState() => ChatCreationDialogState();
}

class ChatCreationDialogState extends State<ChatCreationDialog> {
  final TextEditingController _themeInputController = TextEditingController();
  final TextEditingController _descInputController = TextEditingController();
  Timer _textChangeTimer;

  @override
  Widget build(BuildContext context) =>
      StoreConnector<AppState, ChatCreationViewModel>(
        rebuildOnChange: false,
        converter: (store) => ChatCreationViewModel.create(store),
        builder: (context, vm) => WillPopScope(
              onWillPop: () => Future.value(false),
              child: CustomAlertDialog(
                title: Text(vm.isFirstPhase ? createNewChat : selectUpToSixUsers),
                content: vm.isFirstPhase ? _contentData(vm) : _contentUsers(vm),
                actions: vm.isFirstPhase ? _actionsData(vm) : _actionsUsers(vm),
              ),
            ),
      );

  /// UI build methods

  Widget _contentData(ChatCreationViewModel vm) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      newChatTheme,
                      style: const TextStyle(
                        color: Colors.lightBlue,
                        fontWeight: FontWeight.w600,
                        fontSize: 18.0,
                      ),
                    ),
                    TextField(
                      controller: _themeInputController,
                      onChanged: _listenInput,
                    ),
                  ]),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      newChatDesc,
                      style: const TextStyle(
                        color: Colors.lightBlue,
                        fontWeight: FontWeight.w600,
                        fontSize: 18.0,
                      ),
                    ),
                    TextField(
                      controller: _descInputController,
                      onChanged: _listenInput,
                    ),
                  ]),
            ),
          ]);

  Widget _contentUsers(ChatCreationViewModel vm) => Container(
        height: 190.0,
        child: Scrollbar(
          child: ListView.builder(
              itemCount: vm.usersCount,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                final User user = vm.getUser(index);
                return UserSelectionWidget(
                    user: user,
                    isSelected: vm.isUserSelected(user.id),
                    isActive: vm.isSelectionActive(user.id),
                    onChangeAction: (id, value) {
                      vm.changeUserSelection(userId: id, addSelection: value);
                      _updateWidget();
                    });
              }),
        ),
      );

  List<Widget> _actionsData(ChatCreationViewModel vm) => [
        FlatButton(
          child: Text(cancel),
          onPressed: () => Navigator.of(context).pop(),
        ),
        FlatButton(
            child: Text(next),
            onPressed: _canChangePhase(vm)
                ? () {
                    vm.changePhase();
                    _updateWidget();
                  }
                : null),
      ];

  List<Widget> _actionsUsers(ChatCreationViewModel vm) => [
        FlatButton(
            child: Text(back),
            onPressed: () {
              vm.changePhase();
              _updateWidget();
            }),
        FlatButton(
          child: Text(confirm),
          onPressed: vm.isSelectionNotEmpty()
              ? () => vm.onCreateAction(
                    context,
                    _theme: _themeInputController.text.trim(),
                    _description: _descInputController.text.trim(),
                    userIds: vm.selectedUserIds,
                  )
              : null,
        ),
      ];

  /// Internal helper methods

  void _listenInput(String input) {
    _textChangeTimer?.cancel();
    _textChangeTimer = Timer(Duration(milliseconds: 50), () => _updateWidget());
  }

  void _updateWidget() => setState(() {});

  bool _canChangePhase(ChatCreationViewModel vm) =>
      vm.canChangePhase(_themeInputController.text, _descInputController.text);
}
