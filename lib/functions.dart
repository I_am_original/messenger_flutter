import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

Future<T> showOwnDialog<T>({
  @required BuildContext context,
  @required WidgetBuilder builder,
}) =>
    showDialog(context: context, builder: builder, barrierDismissible: false);
