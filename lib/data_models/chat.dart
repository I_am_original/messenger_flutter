import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:uuid/uuid.dart';

part 'chat.g.dart';

abstract class Chat implements Built<Chat, ChatBuilder> {
  static String columnId = "_id";
  static String columnTheme = "theme";
  static String columnDescription = "description";

  static Serializer<Chat> get serializer => _$chatSerializer;

  String get id; // UUID
  String get theme; // Name of chat and its ID
  String get description;

  BuiltList<String> get userIds; // List of user ID's

  factory Chat([updates(ChatBuilder b)]) = _$Chat;

  Chat._();

  Map<String, dynamic> toMap() => {
        columnId: id ?? Uuid().v1(),
        columnTheme: theme,
        columnDescription: description,
      };

  List<Map<String, dynamic>> usersIdsFromChatList() {
    List<Map<String, dynamic>> chatUserList = [];
    userIds.forEach((userId) => chatUserList.add({
          UserIdsFromChat.columnId: userId + id,
          UserIdsFromChat.columnChatId: id,
          UserIdsFromChat.columnUserId: userId,
        }));
    return chatUserList;
  }

  static Chat fromMap(
          Map<String, dynamic> map, List<Map<String, dynamic>> userIdsList) =>
      Chat((builder) => builder
        ..id = map[columnId]
        ..theme = map[columnTheme]
        ..description = map[columnDescription]
        ..userIds.replace(UserIdsFromChat.fromList(userIdsList)));
}

abstract class UserIdsFromChat {
  static String columnId = "_id";
  static String columnChatId = "chat_id";
  static String columnUserId = "user_id";

  // if Needed we can add some extra data for this table like user nick for chat

  static List<String> fromList(List<Map<String, dynamic>> listOfMaps) {
    List<String> userIds = [];
    listOfMaps.forEach((map) => userIds.add(map[UserIdsFromChat.columnUserId]));
    return userIds;
  }
}
