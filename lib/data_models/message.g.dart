// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<Message> _$messageSerializer = new _$MessageSerializer();

class _$MessageSerializer implements StructuredSerializer<Message> {
  @override
  final Iterable<Type> types = const [Message, _$Message];
  @override
  final String wireName = 'Message';

  @override
  Iterable serialize(Serializers serializers, Message object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'text',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
      'userId',
      serializers.serialize(object.userId,
          specifiedType: const FullType(String)),
      'chatId',
      serializers.serialize(object.chatId,
          specifiedType: const FullType(String)),
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Message deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MessageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'text':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userId':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'chatId':
          result.chatId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Message extends Message {
  @override
  final String id;
  @override
  final String text;
  @override
  final String userId;
  @override
  final String chatId;
  @override
  final String timestamp;

  factory _$Message([void updates(MessageBuilder b)]) =>
      (new MessageBuilder()..update(updates)).build();

  _$Message._({this.id, this.text, this.userId, this.chatId, this.timestamp})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Message', 'id');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('Message', 'text');
    }
    if (userId == null) {
      throw new BuiltValueNullFieldError('Message', 'userId');
    }
    if (chatId == null) {
      throw new BuiltValueNullFieldError('Message', 'chatId');
    }
    if (timestamp == null) {
      throw new BuiltValueNullFieldError('Message', 'timestamp');
    }
  }

  @override
  Message rebuild(void updates(MessageBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MessageBuilder toBuilder() => new MessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Message &&
        id == other.id &&
        text == other.text &&
        userId == other.userId &&
        chatId == other.chatId &&
        timestamp == other.timestamp;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), text.hashCode), userId.hashCode),
            chatId.hashCode),
        timestamp.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Message')
          ..add('id', id)
          ..add('text', text)
          ..add('userId', userId)
          ..add('chatId', chatId)
          ..add('timestamp', timestamp))
        .toString();
  }
}

class MessageBuilder implements Builder<Message, MessageBuilder> {
  _$Message _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  String _userId;
  String get userId => _$this._userId;
  set userId(String userId) => _$this._userId = userId;

  String _chatId;
  String get chatId => _$this._chatId;
  set chatId(String chatId) => _$this._chatId = chatId;

  String _timestamp;
  String get timestamp => _$this._timestamp;
  set timestamp(String timestamp) => _$this._timestamp = timestamp;

  MessageBuilder();

  MessageBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _text = _$v.text;
      _userId = _$v.userId;
      _chatId = _$v.chatId;
      _timestamp = _$v.timestamp;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Message other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Message;
  }

  @override
  void update(void updates(MessageBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Message build() {
    final _$result = _$v ??
        new _$Message._(
            id: id,
            text: text,
            userId: userId,
            chatId: chatId,
            timestamp: timestamp);
    replace(_$result);
    return _$result;
  }
}

class _$MessageEntry extends MessageEntry {
  @override
  final String id;
  @override
  final String authorName;
  @override
  final String messageContent;
  @override
  final String dateTime;
  @override
  final bool isMine;

  factory _$MessageEntry([void updates(MessageEntryBuilder b)]) =>
      (new MessageEntryBuilder()..update(updates)).build();

  _$MessageEntry._(
      {this.id,
      this.authorName,
      this.messageContent,
      this.dateTime,
      this.isMine})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('MessageEntry', 'id');
    }
    if (authorName == null) {
      throw new BuiltValueNullFieldError('MessageEntry', 'authorName');
    }
    if (messageContent == null) {
      throw new BuiltValueNullFieldError('MessageEntry', 'messageContent');
    }
    if (dateTime == null) {
      throw new BuiltValueNullFieldError('MessageEntry', 'dateTime');
    }
    if (isMine == null) {
      throw new BuiltValueNullFieldError('MessageEntry', 'isMine');
    }
  }

  @override
  MessageEntry rebuild(void updates(MessageEntryBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MessageEntryBuilder toBuilder() => new MessageEntryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MessageEntry &&
        id == other.id &&
        authorName == other.authorName &&
        messageContent == other.messageContent &&
        dateTime == other.dateTime &&
        isMine == other.isMine;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, id.hashCode), authorName.hashCode),
                messageContent.hashCode),
            dateTime.hashCode),
        isMine.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MessageEntry')
          ..add('id', id)
          ..add('authorName', authorName)
          ..add('messageContent', messageContent)
          ..add('dateTime', dateTime)
          ..add('isMine', isMine))
        .toString();
  }
}

class MessageEntryBuilder
    implements Builder<MessageEntry, MessageEntryBuilder> {
  _$MessageEntry _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _authorName;
  String get authorName => _$this._authorName;
  set authorName(String authorName) => _$this._authorName = authorName;

  String _messageContent;
  String get messageContent => _$this._messageContent;
  set messageContent(String messageContent) =>
      _$this._messageContent = messageContent;

  String _dateTime;
  String get dateTime => _$this._dateTime;
  set dateTime(String dateTime) => _$this._dateTime = dateTime;

  bool _isMine;
  bool get isMine => _$this._isMine;
  set isMine(bool isMine) => _$this._isMine = isMine;

  MessageEntryBuilder();

  MessageEntryBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _authorName = _$v.authorName;
      _messageContent = _$v.messageContent;
      _dateTime = _$v.dateTime;
      _isMine = _$v.isMine;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MessageEntry other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MessageEntry;
  }

  @override
  void update(void updates(MessageEntryBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$MessageEntry build() {
    final _$result = _$v ??
        new _$MessageEntry._(
            id: id,
            authorName: authorName,
            messageContent: messageContent,
            dateTime: dateTime,
            isMine: isMine);
    replace(_$result);
    return _$result;
  }
}
