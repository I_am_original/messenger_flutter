library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:synchronized/synchronized.dart';

part 'serializers.g.dart';

/// Collection of generated serializers for the built_value.
@SerializersFor(const [
  Chat,
  User,
  Message,
])
final Serializers serializers = _$serializers;
