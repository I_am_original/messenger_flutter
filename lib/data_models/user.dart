import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';

part 'user.g.dart';

@immutable
abstract class User implements Built<User, UserBuilder> {
  static String columnId = "_id";
  static String columnFirstName = "first_name";
  static String columnSecondName = "second_name";

  static Serializer<User> get serializer => _$userSerializer;

  String get id; // User UUID
  String get firstName; // User given name
  String get secondName; // User family name

  String get abbreviation =>
      "${firstName[0]}${secondName[0]}".toUpperCase(); // User nick name
  String get fullName => "$firstName $secondName";

  factory User([updates(UserBuilder b)]) = _$User;

  User._();

  Map<String, dynamic> toMap() => {
        columnId: id ?? Uuid().v1(),
        columnFirstName: firstName,
        columnSecondName: secondName,
      };

  static User fromMap(Map<String, dynamic> map) => User((builder) => builder
    ..id = map[columnId]
    ..firstName = map[columnFirstName]
    ..secondName = map[columnSecondName]);
}
