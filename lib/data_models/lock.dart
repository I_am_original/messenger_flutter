import 'package:meta/meta.dart';

enum LockState { IDLE, TRIGGERED, SHOWN }

class Lock {
  static String columnId = "_id";
  static String columnIsLocked = "is_locked";
  static String columnIsEnabled = "is_enabled";

  final bool isEnabled; // is Lock enabled in settings
  final LockState lockState;

  Lock({
    @required this.isEnabled,
    @required this.lockState,
  });

  Map<String, dynamic> toMap() => {
        columnId: "single",
        columnIsEnabled: isEnabled ? 1 : 0,
        columnIsLocked: lockState == LockState.IDLE ? 0 : 1,
      };

  static Lock fromMap(Map<String, dynamic> map) => Lock(
        isEnabled: map[columnIsEnabled] == 1 ? true : false,
        lockState: map[columnIsLocked] == 1 ? LockState.TRIGGERED : LockState.IDLE,
      );
}
