import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';

part 'message.g.dart';

@immutable
abstract class Message implements Built<Message, MessageBuilder> {
  static String columnId = "_id";
  static String columnText = "text";
  static String columnUserId = "user_id";
  static String columnChatId = "chat_id";
  static String columnTimestamp = "timestamp";

  static Serializer<Message> get serializer => _$messageSerializer;

  String get id; // Message UUID
  String get text; // Message text
  String get userId; // UserID
  String get chatId; // ChatID
  String get timestamp; // Serves as Time

  factory Message([updates(MessageBuilder b)]) = _$Message;

  Message._();

  Map<String, dynamic> toMap() => {
        columnId: id ?? Uuid().v1(),
        columnText: text,
        columnUserId: userId,
        columnChatId: chatId,
        columnTimestamp: timestamp,
      };

  static Message fromMap(Map<String, dynamic> map) =>
      Message((builder) => builder
        ..id = map[columnId]
        ..text = map[columnText]
        ..userId = map[columnUserId]
        ..chatId = map[columnChatId]
        ..timestamp = map[columnTimestamp]);
}

@immutable
abstract class MessageEntry
    implements Built<MessageEntry, MessageEntryBuilder> {
  String get id; // Message UUID
  String get authorName; // Message author name - User ID
  String get messageContent; // Message content
  String get dateTime; // Message date and time
  bool get isMine; // check if author is me

  factory MessageEntry([updates(MessageEntryBuilder b)]) = _$MessageEntry;

  MessageEntry._();
}
