const String routeHomeScreen = "/home_screen";
const String routeLockScreen = "/lock_screen";
const String routeChatScreen = "/chat_screen";
const String routeCameraScreen = "/camera_screen";
const String routeGalleryScreen = "/gallery_screen";

const String inputHint = "Send a message";
const String noUserText = "No user name";
const String noMessageText = "No message";
const String dateFormat = "HH:mm:ss yyyy.MM.dd";
const String timeFormat = "HH:mm:ss.ms";
const String myProfileName = "Vadym Pinchuk";
const String myNickName = "raccoon";
const String myProfileId = "30.08.1989";

const String emptyChatListText = "No chats at the moment";
const String emptyMessagesListText = "This chat is blank now";
const String appTitle = "Flutter Messenger";
const String closeApp = "Close application";
const String settingsLock = "Lock enabler";
const String settingsTheme = "Dark theme switch";
const String settingsLockEnabled = "Lock screen enabled";
const String settingsLockDisabled = "Lock screen disabled";
const String settings = "Settings";

const String camera = "Camera";
const String gallery = "Gallery";

// Lock screen
const String unlock = "UNLOCK";
// Dialog
const String dialogTitle = "Delete message";
const String next = "NEXT";
const String back = "BACK";
const String done = "DONE";
const String cancel = "CANCEL";
const String confirm = "CONFIRM";
const String createNewChat = "Create new chat";
const String newChatTheme = "New chat theme";
const String newChatDesc = "New chat description";
const String selectUpToSixUsers = "Select up to 6 users";

String dialogMessage(String authorName) =>
    "You really want to delete message from $authorName?";
