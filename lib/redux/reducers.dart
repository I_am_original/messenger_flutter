import 'dart:collection';

import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:redux/redux.dart';

/// State transforming function by some action

final messengerReducers = combineReducers<AppState>([
  TypedReducer<AppState, LoaderDisplayingAction>(_loadingStartReducer),
  TypedReducer<AppState, LoaderHidingAction>(_loadingEndReducer),

  TypedReducer<AppState, LockChangesAction>(_lockActionReducer),
  TypedReducer<AppState, ThemeChangesAction>(_themeActionReducer),

  TypedReducer<AppState, UserCreatedAction>(_addUserReducer),
  TypedReducer<AppState, ChatCreatedAction>(_addChatReducer),
  TypedReducer<AppState, MessageCreatedAction>(_addMessageReducer),
  TypedReducer<AppState, OpenChatAction>(_openChatReducer),

  /// State update with data from Database
  TypedReducer<AppState, UsersDownloadedAction>(_insertUsersReducer),
  TypedReducer<AppState, ChatsDownloadedAction>(_insertChatsReducer),
  TypedReducer<AppState, MessagesDownloadedAction>(_insertMessagesReducer),
  TypedReducer<AppState, MessageDeleteAction>(_deleteMessage),
]);

/// Loading indicator handling

AppState _loadingStartReducer(AppState state, LoaderDisplayingAction action) =>
    _loadingStatusReducer(true, state);

AppState _loadingEndReducer(AppState state, LoaderHidingAction action) =>
    _loadingStatusReducer(false, state);

AppState _loadingStatusReducer(bool loadingStatus, AppState state) =>
    state.copyWith(isLoading: loadingStatus);

/// Theme flag handling

AppState _themeActionReducer(AppState state, ThemeChangesAction action) =>
    state.copyWith(isDarkTheme: action.isDarkTheme);

/// Lock screen handling

AppState _lockActionReducer(AppState state, LockChangesAction action) =>
    state.copyWith(lock: action.lock);

/// Chat reducers

AppState _addChatReducer(AppState state, ChatCreatedAction action) =>
    _saveChat(state, {action.chat.id: action.chat});

AppState _insertChatsReducer(AppState state, ChatsDownloadedAction action) =>
    _saveChat(state, action.chatsMap);

AppState _saveChat(AppState state, Map<String, Chat> chatsMap) =>
    state.copyWith(chatsMap: {}..addAll(state.chatsMap)..addAll(chatsMap));

AppState _openChatReducer(AppState state, OpenChatAction action) =>
    state.copyWith(chatId: action.chatId);

/// Message reducers

AppState _addMessageReducer(AppState state, MessageCreatedAction action) =>
    _saveMessages(state, {action.message.id: action.message});

AppState _insertMessagesReducer(
        AppState state, MessagesDownloadedAction action) =>
    _saveMessages(state, action.messagesMap);

AppState _saveMessages(AppState state, Map<String, Message> messagesMap) =>
    state.copyWith(
        messagesMap: SplayTreeMap()
          ..addAll(state.messagesMap)
          ..addAll(messagesMap));

AppState _deleteMessage(AppState state, MessageDeleteAction action) =>
    state.copyWith(
        messagesMap: SplayTreeMap()
          ..addAll(state.messagesMap)
          ..remove(action.messageId));

/// User reducers

AppState _addUserReducer(AppState state, UserCreatedAction action) =>
    _saveUsers(state, {action.user.id: action.user});

AppState _insertUsersReducer(AppState state, UsersDownloadedAction action) =>
    _saveUsers(state, action.usersMap);

AppState _saveUsers(AppState state, Map<String, User> usersMap) =>
    state.copyWith(usersMap: {}..addAll(state.usersMap)..addAll(usersMap));
