import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:meta/meta.dart';

// Flag action for showing loading indicator
abstract class LoaderDisplayingAction {}

// Flag action for hiding loading indicator
abstract class LoaderHidingAction {}

// Flag action for starting random message generation
abstract class MessageGenerationAction {}

// Action thrown on app start for opening all needed connection on the start
// TODO: remove when it will be useless
class AppStartedAction {}

// Trigger action for loading Users and Chats from DB
class DatabaseOpenedAction implements LoaderDisplayingAction {}

// General action sent on each Add click
class AddClickedAction {}

// Flag action - if DB empty or Just created - no data loaded
class EmptyPayloadAction implements LoaderHidingAction {}

/// Chat actions

class CreateChatAction {
  final String theme;
  final String description;
  final List<String> selectedUserIds;

  CreateChatAction({
    @required this.theme,
    @required this.description,
    @required this.selectedUserIds,
  });
}

class OpenChatAction {
  final String chatId;

  OpenChatAction(this.chatId);
}

class ChatOpenedAction implements LoaderDisplayingAction {}

class ChatCreatedAction implements MessageGenerationAction {
  final Chat chat;

  ChatCreatedAction(this.chat);
}

// Action thrown on db open finish
class ChatsDownloadedAction implements MessageGenerationAction {
  Map<String, Chat> chatsMap;

  ChatsDownloadedAction(this.chatsMap);
}

/// User actions

abstract class UsersAmountAction {
  int get amount;
}

class UsersEmptyAction implements EmptyPayloadAction, UsersAmountAction {
  @override
  int get amount => 0;
}

// Action thrown on db open finish
class UsersDownloadedAction implements UsersAmountAction {
  Map<String, User> usersMap;

  UsersDownloadedAction(this.usersMap);

  @override
  int get amount => usersMap.length;
}

class UserCreatedAction {
  final User user;

  UserCreatedAction(this.user);
}

/// Message Actions

class MessageCreatedAction {
  final Message message;

  MessageCreatedAction(this.message);
}

// Action thrown on chats loading finish
class MessagesDownloadedAction
    implements MessageGenerationAction, LoaderHidingAction {
  Map<String, Message> messagesMap;

  MessagesDownloadedAction(this.messagesMap);
}

class MessageDeleteAction {
  final String messageId;

  MessageDeleteAction(this.messageId);
}

/// Lock Actions

class LockChangesAction {
  final Lock lock;

  LockChangesAction(this.lock);
}

/// Theme flag change Action

class ThemeChangesAction {
  final bool isDarkTheme;

  ThemeChangesAction(this.isDarkTheme);
}
