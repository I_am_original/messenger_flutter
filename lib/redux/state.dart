import 'dart:collection';

import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:meta/meta.dart';

@immutable
class AppState {
  final bool isLoading;
  final bool isDarkTheme;
  final Lock lock;
  final String chatId; // Currently opened chat
  final String profileId; // Name of user from profile
  final Map<String, Chat> chatsMap; // All chats accessible by ID (UUID)
  final Map<String, User> usersMap; // All users accessible by ID (UUID)
  final Map<String, Message> messagesMap; // All messages by ID (UUID)

  // Constructor with optional param initializer - synthetic sugar
  AppState({
    this.isLoading = false,
    this.isDarkTheme = false,
    @required this.lock,
    @required this.chatId,
    @required this.profileId,
    @required this.chatsMap,
    @required this.usersMap,
    @required this.messagesMap,
  });

  // Named constructor with initializer list after : sign
  AppState.initial()
      : isLoading = false,
        isDarkTheme = false,
        lock = Lock(
          isEnabled: true,
          lockState: LockState.IDLE,
        ),
        chatId = "",
        profileId = myProfileId,
        chatsMap = {},
        usersMap = {},
        messagesMap = SplayTreeMap();

  AppState copyWith({
    bool isLoading,
    bool isDarkTheme, // TODO: should be in sharedPrefs
    Lock lock,
    String chatId,
    String profileId,
    Map<String, Chat> chatsMap,
    Map<String, User> usersMap,
    Map<String, Message> messagesMap,
  }) {
    return AppState(
      isLoading: isLoading ?? this.isLoading,
      isDarkTheme: isDarkTheme ?? this.isDarkTheme,
      lock: lock ?? this.lock,
      chatId: chatId ?? this.chatId,
      profileId: profileId ?? this.profileId,
      chatsMap: chatsMap ?? this.chatsMap,
      usersMap: usersMap ?? this.usersMap,
      messagesMap: messagesMap ?? this.messagesMap,
    );
  }
}
