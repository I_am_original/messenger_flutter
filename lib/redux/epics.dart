import 'dart:async';
import 'dart:math';

import 'package:english_words/english_words.dart';
import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/database/db_connector.dart';
import 'package:flutter_messenger/database/db_interface.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';

import 'selectors.dart';

final messengerEpics = combineEpics<AppState>([
  /// Database work epics
  TypedEpic<AppState, UserCreatedAction>(saveUserToDb(DatabaseConnector())),
  TypedEpic<AppState, MessageCreatedAction>(
      saveMessageToDb(DatabaseConnector())),
  TypedEpic<AppState, ChatCreatedAction>(saveChatToDb(DatabaseConnector())),
  TypedEpic<AppState, DatabaseOpenedAction>(
      readUsersFromDb(DatabaseConnector())),
  TypedEpic<AppState, DatabaseOpenedAction>(
      readChatsFromDb(DatabaseConnector())),
  TypedEpic<AppState, DatabaseOpenedAction>(
      readLastChatsMessagesFromDb(DatabaseConnector())),
  TypedEpic<AppState, ChatOpenedAction>(
      readChatMessagesFromDb(DatabaseConnector())),
  TypedEpic<AppState, MessageDeleteAction>(
      deleteChatMessagesFromDb(DatabaseConnector())),

  /// Lock actions
  TypedEpic<AppState, LockChangesAction>(saveLockToDb(DatabaseConnector())),

  /// Random data generation epics
  TypedEpic<AppState, UsersAmountAction>(createUsers),
  TypedEpic<AppState, MessageGenerationAction>(messageGenerationEpic),

  /// Creation of chat by user epic
  TypedEpic<AppState, CreateChatAction>(createChat),
  TypedEpic<AppState, ChatCreatedAction>(openChat),

  TypedEpic<AppState, LockChangesAction>(lockTriggeringEpic),
]);

/// Database work epics - functions return Epics as an anonymous function

Epic<AppState> openDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => connector
        .openDB()
        .firstWhere((db) => db != null && db.isOpen)
        .asObservable()
        .map((db) => DatabaseOpenedAction());

/// Lock epics

Epic<AppState> saveLockToDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .map((action) => action.lock)
        .flatMap((lock) => connector.saveLock(lock));

Epic<AppState> getLockFromDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) =>
        Observable(actions).flatMap((action) => connector.getLock().map(
              (lock) => LockChangesAction(
                    lock ?? Lock(isEnabled: true, lockState: LockState.IDLE),
                  ),
            ));

/// Saving to DB

Epic<AppState> saveUserToDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .map((action) => action.user)
        .flatMap((user) => connector.insertUser(user));

Epic<AppState> saveMessageToDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .map((action) => action.message)
        .flatMap((message) => connector.insertMessage(message));

Epic<AppState> saveChatToDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .map((action) => action.chat)
        .flatMap((chat) => connector.insertChat(chat));

/// Reading from DB

Epic<AppState> readUsersFromDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .flatMap((action) => connector.getAllUsers().map((usersMap) =>
            usersMap.isEmpty
                ? UsersEmptyAction()
                : UsersDownloadedAction(usersMap)));

Epic<AppState> readChatsFromDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .flatMap((action) => connector.getAllChats().map((chatsMap) =>
            chatsMap.isEmpty
                ? EmptyPayloadAction()
                : ChatsDownloadedAction(chatsMap)));

Epic<AppState> readLastChatsMessagesFromDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .flatMap((action) => connector.getLastMessagesForChats().map(
            (messagesMap) => messagesMap.isEmpty
                ? EmptyPayloadAction()
                : MessagesDownloadedAction(messagesMap)));

Epic<AppState> readChatMessagesFromDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .flatMap((action) => connector
            .getMessagesForChat(openedChatIdSelector(store.state))
            .map((messagesMap) => messagesMap.isEmpty
                ? EmptyPayloadAction()
                : MessagesDownloadedAction(messagesMap)));

/// Deleting from DB

Epic<AppState> deleteChatMessagesFromDb(ConnectorInterface connector) =>
    (Stream<dynamic> actions, EpicStore<AppState> store) => Observable(actions)
        .flatMap((action) => connector.deleteMessage(action.messageId));

/// Random data generation epics

Observable<dynamic> addActionEpic(
        Stream<dynamic> actions, EpicStore<AppState> epicStore) =>
    Observable(actions).flatMap((a) => Observable.concat(
        [createUsers(actions, epicStore), _createRandomChat(epicStore)]));

Observable<dynamic> createUsers(
        Stream<UsersAmountAction> actions, EpicStore<AppState> epicStore) =>
    Observable(actions).take(1).flatMap(
          (action) => action.amount == 0
              ? Observable.concat([
                  Observable.fromIterable(generateWordPairs().take(13).map(
                        (pair) => UserCreatedAction(
                              User(
                                (builder) => builder
                                  ..id = Uuid().v1()
                                  ..firstName = pair.first
                                  ..secondName = pair.second,
                              ),
                            ),
                      )),
                  _createProfile(epicStore)
                ])
              : Observable.empty(),
        );

Observable<dynamic> _createProfile(EpicStore<AppState> epicStore) =>
    Observable.just(profileIdSelector(epicStore.state)).map(
      (id) => UserCreatedAction(
            User(
              (builder) => builder
                ..id = id
                ..firstName = myProfileName.split(" ")[0]
                ..secondName = myProfileName.split(" ")[1],
            ),
          ),
    );

Observable<dynamic> _createRandomChat(EpicStore<AppState> epicStore) =>
    Observable.just(epicStore)
        .map(
          (store) => Chat(
                (builder) => builder
                  ..id = Uuid().v1()
                  ..theme = WordPair.random().asPascalCase
                  ..description = WordPair.random().asString
                  ..userIds.replace(([]
                    ..addAll(otherUserIdsListSelector(store.state)
                        .take(Random().nextInt(6)))
                    ..add(profileIdSelector(store.state)))),
              ),
        )
        .map((chat) => ChatCreatedAction(chat));

Observable<dynamic> createChat(
        Stream<CreateChatAction> actions, EpicStore<AppState> epicStore) =>
    Observable(actions).map(
      (action) => ChatCreatedAction(
            Chat(
              (builder) => builder
                ..id = Uuid().v1()
                ..theme = action.theme
                ..description = action.description
                ..userIds.replace(action.selectedUserIds),
            ),
          ),
    );

Observable<dynamic> openChat(
        Stream<ChatCreatedAction> actions, EpicStore<AppState> epicStore) =>
    Observable(actions).map((action) => OpenChatAction(action.chat.id));

/// Start random generation of messages while first chat created or messages loaded from DB

Observable<dynamic> messageGenerationEpic(
        Stream<dynamic> actions, EpicStore<AppState> epicStore) =>
    Observable(actions)
        .take(1)
        .flatMap((action) => Observable.periodic(
            Duration(seconds: 3), (i) => Random().nextInt(100)))
        .flatMap((delay) => Observable.just(epicStore)
            .delay(Duration(seconds: delay))
            .map((state) => Message((builder) {
                  var timestampId = DateTime.now().toString();
                  var chatId = chatIdsListSelector(epicStore.state)[
                      Random().nextInt(chatsCountSelector(epicStore.state))];
                  var userIdsList =
                      chatsMapSelector(epicStore.state)[chatId].userIds;
                  builder
                    ..id = timestampId
                    ..text = generateWordPairs()
                        .take(Random().nextInt(15))
                        .toString()
                    ..userId = userIdsList[Random().nextInt(userIdsList.length)]
                    ..chatId = chatId
                    ..timestamp = timestampId;
                }))
            .map((message) => MessageCreatedAction(message)));

/// Emit event about locking screen

Observable<dynamic> lockTriggeringEpic(
        Stream<LockChangesAction> actions, EpicStore<AppState> epicStore) =>
    Observable(actions)
        .map((action) => action.lock)
        .where((lock) => lock.isEnabled && lock.lockState == LockState.IDLE)
        .switchMap((lock) => Observable.just(epicStore)
            .delay(Duration(seconds: 15))
            .where((store) =>
                isLockEnabledSelector(store.state) &&
                isLockIdleSelector(store.state))
            .map(
              (store) => LockChangesAction(
                    Lock(
                      isEnabled: isLockEnabledSelector(store.state),
                      lockState: LockState.TRIGGERED,
                    ),
                  ),
            ));
