import 'dart:collection';

import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:reselect/reselect.dart';

/// LOADING status selector

final loadingStatusSelector = (AppState state) => state.isLoading;

/// DARK THEME FLAG selector

final themeFlagSelector = (AppState state) => state.isDarkTheme;

/// LOCK settings/state selectors

final lockSelector = (AppState state) => state.lock;

final lockStateSelector =
    createSelector1(lockSelector, (Lock lock) => lock.lockState);

final isLockEnabledSelector =
    createSelector1(lockSelector, (Lock lock) => lock.isEnabled);

final isLockIdleSelector = createSelector1(
    lockStateSelector, (LockState lockState) => lockState == LockState.IDLE);

final isLockTriggeredSelector = createSelector1(lockStateSelector,
    (LockState lockState) => lockState == LockState.TRIGGERED);

final isLockShownSelector = createSelector1(
    lockStateSelector, (LockState lockState) => lockState == LockState.SHOWN);

/// PROFILE ID selectors

final profileIdSelector = (AppState state) => state.profileId;

/// OPENED chat selectors

final openedChatIdSelector = (AppState state) => state.chatId;

final openedChatThemeSelector =
    createSelector1(openedChatSelector, (Chat chat) => chat.theme);

final openedChatSelector = createSelector2(
    chatsMapSelector,
    openedChatIdSelector,
    (Map<String, Chat> chats, String chatId) => chats[chatId]);

/// MAP selectors

final chatsMapSelector = (AppState state) => state.chatsMap;

final messagesMapSelector = (AppState state) => state.messagesMap;

final allUsersMapSelector = (AppState state) => state.usersMap;

final profileUserSelector = createSelector2(
    profileIdSelector,
    allUsersMapSelector,
    (String profileId, Map<String, User> allUsers) => allUsers[profileId]);

final otherUsersMapSelector = createSelector2(
    profileIdSelector,
    allUsersMapSelector,
    (String profileId, Map<String, User> allUsers) =>
        LinkedHashMap<String, User>()
          ..addAll(allUsers)
          ..removeWhere((key, value) => profileId == key));

/// VALUE LIST selectors

final chatsListSelector = createSelector1(
    chatsMapSelector, (Map<String, Chat> chats) => chats.values.toList());

final messagesListSelector = createSelector1(messagesMapSelector,
    (Map<String, Message> messages) => messages.values.toList());

final allUsersListSelector = createSelector1(
    allUsersMapSelector, (Map<String, User> users) => users.values.toList());

final otherUsersListSelector = createSelector1(
    otherUsersMapSelector, (Map<String, User> users) => users.values.toList());

/// KEY List selectors

final chatIdsListSelector = createSelector1(
    chatsMapSelector, (Map<String, Chat> chats) => chats.keys.toList());

final messageIdsListSelector = createSelector1(messagesMapSelector,
    (Map<String, Message> messages) => messages.keys.toList());

final allUserIdsListSelector = createSelector1(
    allUsersMapSelector, (Map<String, User> users) => users.keys.toList());

final otherUserIdsListSelector = createSelector1(
    otherUsersMapSelector, (Map<String, User> users) => users.keys.toList());

/// COUNT selectors

final chatsCountSelector = createSelector1(
    chatsMapSelector, (Map<String, Chat> chats) => chats.length);

final allUsersCountSelector = createSelector1(
    allUsersMapSelector, (Map<String, User> users) => users.length);

final otherUsersCountSelector = createSelector1(
    otherUsersMapSelector, (Map<String, User> users) => users.length);

final messagesCountSelector = createSelector1(
    messagesMapSelector, (Map<String, Message> messages) => messages.length);

/// Custom selectors

final homeTitleSelector = createSelector1(chatsCountSelector,
    (int count) => count == 0 ? appTitle : "Messenger Chats: $count");
