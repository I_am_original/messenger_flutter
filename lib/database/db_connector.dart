import 'dart:async';

import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/database/db_interface.dart';
import 'package:flutter_messenger/main.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sqflite/sqflite.dart';

/// Implementation of Database Connector with business logic for this

class DatabaseConnector extends ConnectorInterface {
  static final DatabaseConnector _instance = new DatabaseConnector._internal();

  @override
  Database database;

  factory DatabaseConnector() => _instance;

  DatabaseConnector._internal();

  /// Lifecycle handling functions

  @override
  Observable<Database> openDB() => Observable.fromFuture(_open());

  @override
  Observable closeDB() => Observable.fromFuture(_close());

  @override
  Observable deleteDB() => Observable.fromFuture(_delete());

  /// Opening, creating DB ///

  get _dbPath async =>
      "${await getDatabasesPath()}/${ConnectorInterface.dbName}";

  Future<Database> _open() async {
    database = await openDatabase(
      await _dbPath,
      version: 1,
      onCreate: (Database db, int version) async {
        final Batch batch = db.batch();
        batch.execute(
            "CREATE TABLE ${ConnectorInterface.lockTable} (${Lock.columnId} TEXT not null PRIMARY KEY, ${Lock.columnIsLocked} INTEGER not null,${Lock.columnIsEnabled} INTEGER not null)");
        batch.execute(
            "CREATE TABLE ${ConnectorInterface.userTable} (${User.columnId} TEXT not null PRIMARY KEY, ${User.columnFirstName} TEXT not null,${User.columnSecondName} TEXT not null)");
        batch.execute(
            "CREATE TABLE ${ConnectorInterface.chatTable} (${Chat.columnId} TEXT not null PRIMARY KEY, ${Chat.columnTheme} TEXT not null, ${Chat.columnDescription} TEXT not null)");
        batch.execute(
            "CREATE TABLE ${ConnectorInterface.chatIdUserIdTable} (${UserIdsFromChat.columnId} TEXT not null PRIMARY KEY, ${UserIdsFromChat.columnChatId} TEXT not null,${UserIdsFromChat.columnUserId} TEXT not null)");
        batch.execute(
            "CREATE TABLE ${ConnectorInterface.messageTable} (${Message.columnId} TEXT not null PRIMARY KEY, ${Message.columnText} TEXT not null, ${Message.columnUserId} TEXT not null, ${Message.columnChatId} TEXT not null, ${Message.columnTimestamp} TEXT not null)");
        await batch.commit();
      },
      onOpen: (Database db) => logger.fine("Database Opened: ${db.path}"),
    );
    return database;
  }

  /// Finishing, DB closing

  Future _close() async => database.close();

  Future _delete() async => deleteDatabase(await _dbPath);
}
