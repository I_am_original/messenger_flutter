import 'dart:async';

import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/main.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sqflite/sqflite.dart';

/// Database connector interface with predefined methods for work with DB
/// All Abstract methods should be done in extensions
abstract class ConnectorInterface {
  static String get dbName => "fm.db";

  static String get lockTable => "lock_table";

  static String get userTable => "user_table";

  static String get chatTable => "chat_table";

  static String get chatIdUserIdTable => "chat_id_user_id_table";

  static String get messageTable => "message_table";

  Database database;

  /// Lifecycle - Abstract methods

  Observable<Database> openDB();

  Observable closeDB();

  Observable deleteDB();

  /// Lock methods

  Observable<int> saveLock(Lock lock) =>
      Observable.fromFuture(_saveLock(database, lock));

  Observable<Lock> getLock() => Observable.fromFuture(_getLock(database));

  /// Insert

  Observable<User> insertUser(User user) =>
      Observable.fromFuture(_insertUser(database, user));

  Observable<Chat> insertChat(Chat chat) =>
      Observable.fromFuture(_insertChat(database, chat));

  Observable<Message> insertMessage(Message message) =>
      Observable.fromFuture(_insertMessage(database, message));

  /// Get - All and ById

  Observable<Map<String, User>> getAllUsers() =>
      Observable.fromFuture(_getUsers(database, null));

  Observable<User> getUserById(String userId) =>
      Observable.fromFuture(_getUsers(database, userId))
          .map((map) => map[userId]);

  Observable<Map<String, Chat>> getAllChats() =>
      Observable.fromFuture(_getChats(database, null));

  Observable<Chat> getChatById(String chatId) =>
      Observable.fromFuture(_getChats(database, chatId))
          .map((map) => map[chatId]);

  Observable<Map<String, Message>> getLastMessagesForChats() =>
      Observable.fromFuture(_getLastMessages(database));

  Observable<Map<String, Message>> getMessagesForChat(String chatId) =>
      Observable.fromFuture(_getMessagesForChat(database, chatId));

  /// Update

  Observable<int> updateUser(User user) =>
      Observable.fromFuture(_updateUser(database, user));

  Observable<int> updateChat(Chat chat) =>
      Observable.fromFuture(_updateChat(database, chat));

  Observable<int> updateMessage(Message message) =>
      Observable.fromFuture(_updateMessage(database, message));

  /// Delete

  Observable<int> deleteMessage(String messageId) =>
      Observable.fromFuture(_deleteMessage(database, messageId));
}

/// Specific record about Lock state

Future<int> _saveLock(Database db, Lock lock) async {
  return await db.insert(ConnectorInterface.lockTable, lock.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<Lock> _getLock(Database db) async {
  var lock;
  List<Map> lockMaps = await db.query(
    ConnectorInterface.lockTable,
    columns: [Lock.columnIsEnabled, Lock.columnIsLocked],
    limit: 1,
  );
  if (lockMaps.isNotEmpty) {
    lock = Lock.fromMap(lockMaps.first);
  }
  return lock;
}

/// Insertion functions

Future<User> _insertUser(Database db, User user) async {
  var id = await db.insert(ConnectorInterface.userTable, user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
  logger.fine("DB Insert User: $id");
  return user;
}

Future<Chat> _insertChat(Database db, Chat chat) async {
  final Batch batch = db.batch();
  chat.usersIdsFromChatList().forEach((map) {
    batch.insert(ConnectorInterface.chatIdUserIdTable, map,
        conflictAlgorithm: ConflictAlgorithm.replace);
  });
  batch.insert(ConnectorInterface.chatTable, chat.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
  await batch.commit();
  logger.fine("DB Insert Chat: ${chat.theme}");
  return chat;
}

Future<Message> _insertMessage(Database db, Message message) async {
  var id = await db.insert(ConnectorInterface.messageTable, message.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
  logger.fine("DB Insert Message: $id");
  return message;
}

/// Reading functions

Future<Map<String, Chat>> _getChats(Database db, String chatId) async {
  final Map<String, Chat> chats = {};
  final List<Map> userIdsMaps = await db.query(
    ConnectorInterface.chatIdUserIdTable,
    columns: [UserIdsFromChat.columnUserId],
    where: chatId != null ? "${UserIdsFromChat.columnChatId} = ?" : null,
    whereArgs: chatId != null ? [chatId] : null,
  );
  final List<Map> chatMaps = await db.query(
    ConnectorInterface.chatTable,
    where: chatId != null ? "${Chat.columnId} = ?" : null,
    whereArgs: chatId != null ? [chatId] : null,
  );
  if (chatMaps.isNotEmpty) {
    chatMaps.forEach((map) {
      var chat = Chat.fromMap(map, userIdsMaps);
      chats.putIfAbsent(chat.id, () => chat);
    });
  }
  return chats;
}

Future<Map<String, User>> _getUsers(Database db, String userId) async {
  final Map<String, User> users = {};
  List<Map> userMaps = await db.query(
    ConnectorInterface.userTable,
    where: userId != null ? "${User.columnId} = ?" : null,
    whereArgs: userId != null ? [userId] : null,
  );
  if (userMaps.isNotEmpty) {
    userMaps.forEach((map) {
      var user = User.fromMap(map);
      users.putIfAbsent(user.id, () => user);
    });
  }
  return users;
}

Future<Map<String, Message>> _getLastMessages(Database db) async {
  Map<String, Message> lastMessagesMap = {};
  List<Map> messagesMaps = await db.query(
    ConnectorInterface.messageTable,
    groupBy: "${Message.columnChatId}",
  );
  if (messagesMaps.isNotEmpty) {
    messagesMaps.forEach((map) {
      var message = Message.fromMap(map);
      lastMessagesMap.putIfAbsent(message.id, () => message);
    });
  }
  return lastMessagesMap;
}

Future<Map<String, Message>> _getMessagesForChat(
    Database db, String chatId) async {
  Map<String, Message> chatMessagesMap = {};
  List<Map> messagesMaps = await db.query(
    ConnectorInterface.messageTable,
    where: "${Message.columnChatId} = ?",
    whereArgs: [chatId],
  );
  if (messagesMaps.isNotEmpty) {
    messagesMaps.forEach((map) {
      var message = Message.fromMap(map);
      chatMessagesMap.putIfAbsent(message.id, () => message);
    });
  }
  return chatMessagesMap;
}

/// Update functions

Future<int> _updateUser(Database db, User user) async {
  return await _update(db, user, ConnectorInterface.userTable, User.columnId);
}

Future<int> _updateChat(Database db, Chat chat) async {
  final Batch batch = db.batch();
  chat.usersIdsFromChatList().forEach((map) async {
    batch.update(ConnectorInterface.chatIdUserIdTable, map,
        where: "${UserIdsFromChat.columnId} = ?",
        whereArgs: [map[UserIdsFromChat.columnId]]);
  });
  await batch.commit();
  return await _update(db, chat, ConnectorInterface.chatTable, Chat.columnId);
}

Future<int> _updateMessage(Database db, Message message) async {
  return await _update(
      db, message, ConnectorInterface.messageTable, Message.columnId);
}

Future<int> _update(
    Database db, dynamic object, String tableName, String columnId) async {
  return await db.update(tableName, object.toMap(),
      where: "$columnId = ?", whereArgs: [object.id]);
}

/// Delete functions - Chat and User can be added, maybe, later

Future<int> _deleteMessage(Database db, String messageId) async {
  return await db.delete(ConnectorInterface.messageTable,
      where: "${Message.columnId}= ?", whereArgs: [messageId]);
}
