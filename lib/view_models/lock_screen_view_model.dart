import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:redux/redux.dart';

/// View model handles unlock event for Lock screen

class LockScreenViewModel {
  final onUnlockClicked;

  LockScreenViewModel._({
    @required this.onUnlockClicked,
  });

  @override
  bool operator ==(o) =>
      o is LockScreenViewModel && onUnlockClicked == o.onUnlockClicked;

  @override
  int get hashCode => onUnlockClicked.hashCode;
}

LockScreenViewModel createLockViewModel(Store<AppState> store) =>
    LockScreenViewModel._(onUnlockClicked: (context) {
      store.dispatch(LockChangesAction(
        Lock(
          isEnabled: isLockEnabledSelector(store.state),
          lockState: LockState.IDLE,
        ),
      ));
      // Switch back or move forward
      final NavigatorState navigator = Navigator.of(context);
      if (navigator.canPop()) {
        navigator.pop();
      } else {
        navigator.pushReplacementNamed(routeHomeScreen);
      }
    });
