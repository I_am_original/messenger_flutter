import 'package:flutter/cupertino.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:quiver/core.dart';
import 'package:redux/redux.dart';

/// View model for List of MessageWidget
class ChatScreenViewModel {
  final String _profileId;
  final List<Message> _messages;
  final List<User> _users;
  final String chatTheme;
  final bool isLoading;
  final onSubmitMessage;
  final onOpenMedia;
  final onDeleteMessage;

  ChatScreenViewModel._(
    this._profileId,
    this._messages,
    this._users, {
    @required this.chatTheme,
    @required this.isLoading,
    @required this.onSubmitMessage,
    @required this.onOpenMedia,
    @required this.onDeleteMessage,
  });

  /// Get count of messages in chat
  int get messagesCount => _messages.length;

  @override
  bool operator ==(o) =>
      o is ChatScreenViewModel &&
      _profileId == o._profileId &&
      _messages == o._messages &&
      _users == o._users &&
      chatTheme == o.chatTheme &&
      isLoading == o.isLoading &&
      onSubmitMessage == o.onSubmitMessage &&
      onDeleteMessage == o.onDeleteMessage;

  @override
  int get hashCode => hash4(_profileId.hashCode, _users.hashCode,
      _messages.hashCode, isLoading.hashCode);

  /// Data object for displaying message in chat
  MessageEntry getMessageEntry({int count, int index}) {
    var message = _messages[count - 1 - index];
    var author = _users.where((user) => message.userId == user.id).first;
    var content = message.text;
    var time = DateFormat(dateFormat).format(DateTime.parse(message.timestamp));
    return MessageEntry((builder) => builder
      ..id = message.id
      ..authorName = "${author.abbreviation} (${author.fullName})"
      ..messageContent = content
      ..dateTime = time
      ..isMine = _profileId == author.id);
  }
}

ChatScreenViewModel createChatScreenViewModel(
        Store<AppState> store, BuildContext context) =>
    ChatScreenViewModel._(
        profileIdSelector(store.state),
        messagesListSelector(store.state)
            .where((msg) => msg.chatId == openedChatIdSelector(store.state))
            .toList(),
        allUsersListSelector(store.state)
            .where((user) =>
                openedChatSelector(store.state).userIds.contains(user.id))
            .toList(),
        chatTheme: openedChatThemeSelector(store.state),
        isLoading: loadingStatusSelector(store.state),
        onSubmitMessage: (input) => store.dispatch(
              MessageCreatedAction(
                Message((builder) {
                  var time = DateTime.now().toString();
                  builder
                    ..id = time
                    ..chatId = openedChatIdSelector(store.state)
                    ..userId = profileIdSelector(store.state)
                    ..text = input
                    ..timestamp = time;
                }),
              ),
            ),
        onOpenMedia: (route) => Navigator.of(context).pushNamed(route),
        onDeleteMessage: (id) => store.dispatch(MessageDeleteAction(id)));
