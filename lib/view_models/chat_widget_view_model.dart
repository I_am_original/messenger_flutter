import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:quiver/core.dart';
import 'package:redux/redux.dart';

/// View model for basic configurations of HomeWidget with List of Chats

class ChatWidgetViewModel {
  final String chatTheme; //name and ID for chat
  final String userName;
  final String lastMessageText;
  final onChatClicked;

  ChatWidgetViewModel._({
    @required this.chatTheme,
    @required this.userName,
    @required this.lastMessageText,
    @required this.onChatClicked,
  });

  @override
  bool operator ==(o) =>
      o is ChatWidgetViewModel &&
      chatTheme == o.chatTheme &&
      userName == o.userName &&
      lastMessageText == o.lastMessageText &&
      onChatClicked == o.onChatClicked;

  @override
  int get hashCode => hash4(chatTheme.hashCode, userName.hashCode,
      lastMessageText.hashCode, onChatClicked.hashCode);
}

ChatWidgetViewModel createChatWidgetViewModel(
    {Store<AppState> store, int index}) {
  var chat = chatsListSelector(store.state)[index];
  //TODO: need to use this from selector. in next updates
  var message = _getLastMessage(store, chat);
  User user;
  if (message != null) {
    user = allUsersMapSelector(store.state)[message.userId];
  }

  return ChatWidgetViewModel._(
    chatTheme: chat.theme,
    userName: user != null ? "${user.fullName} (${user.abbreviation})" : noUserText,
    lastMessageText: message != null
        ? message.text
            .substring(0, message.text.length > 139 ? 139 : message.text.length)
        : noMessageText,
    onChatClicked: (context) {
      store.dispatch(OpenChatAction(chat.id));
      Navigator.of(context).pushNamed(routeChatScreen);
      store.dispatch(ChatOpenedAction());
    },
  );
}

Message _getLastMessage(Store store, Chat chat) =>
    messagesListSelector(store.state)
        .lastWhere((message) => message.chatId == chat.id, orElse: () => null);
