import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/widgets/screens/chat_screen.dart';
import 'package:redux/redux.dart';

/// View model with BL for creating chat

class ChatCreationViewModel {
  bool _isFirstPhase = true; // used for dialog only
  List<String> _selectedUserIds = [];
  final Map<String, User> _usersMap;
  final List<User> _usersList;
  final onCreateAction;

  ChatCreationViewModel._(
    this._selectedUserIds,
    this._usersMap,
    this._usersList,
    this.onCreateAction,
  );

  @override
  bool operator ==(o) =>
      o is ChatCreationViewModel &&
      _isFirstPhase == o._isFirstPhase &&
      _selectedUserIds == o.selectedUserIds &&
      _usersMap == o._usersMap &&
      _usersList == o._usersList &&
      onCreateAction == o.onCreateAction;

  @override
  int get hashCode => onCreateAction.hashCode;

  int get usersCount => _usersList.length;

  User getUser(int index) => _usersList[index];

  User getSelectedUser(int index) => _usersMap[_selectedUserIds[index]];

  bool get isFirstPhase => _isFirstPhase;

  void changePhase() => _isFirstPhase = !_isFirstPhase;

  int selectionCount() => _selectedUserIds.length;

  bool canSelectUsers() => selectionCount() < 3;

  bool isUserSelected(String userId) => _selectedUserIds.contains(userId);

  List<String> get selectedUserIds => _selectedUserIds;

  bool canChangePhase(String theme, String description) =>
      theme.isNotEmpty && description.isNotEmpty;

  bool isSelectionNotEmpty() => _selectedUserIds.isNotEmpty;

  bool isSelectionActive(String userId) =>
      isUserSelected(userId) || canSelectUsers();

  void changeUserSelection({
    @required String userId,
    bool addSelection = true,
  }) {
    if (addSelection) {
      _selectedUserIds.add(userId);
    } else {
      _selectedUserIds.remove(userId);
    }
  }

  static ChatCreationViewModel create(Store<AppState> store,
          [List<String> selection]) =>
      ChatCreationViewModel._(
        selection,
        otherUsersMapSelector(store.state),
        otherUsersListSelector(store.state),
        (BuildContext context,
            {String theme, String description, List<String> userIds}) {
          final List<String> allParticipantsIds = []
            ..addAll(userIds)
            ..add(profileIdSelector(store.state));
          store.dispatch(
            CreateChatAction(
              theme: theme,
              description: description,
              selectedUserIds: allParticipantsIds,
            ),
          );
          Timer(
            const Duration(milliseconds: 200),
            () => Navigator.pushReplacement(
                  context,
                  CupertinoPageRoute(builder: (context) => ChatScreen()),
                ),
          );
          // here we can avoid [ChatOpenedAction] as it is empty chat for now
        },
      );
}
