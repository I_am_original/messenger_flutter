import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/widgets/screens/chat_creation_users_screen.dart';
import 'package:quiver/core.dart';
import 'package:redux/redux.dart';

/// View model for basic configurations of HomeWidget with List of Chats

class HomeScreenViewModel {
  final bool isLoading;
  final bool isLockEnabled;
  final bool isDarkTheme;
  final String appBarTitle;
  final int chatListSize;
  final onLockSettingsChanged;
  final onThemeFlagChange;
  final onAddClicked;

  HomeScreenViewModel._({
    @required this.isLoading,
    @required this.isLockEnabled,
    @required this.isDarkTheme,
    @required this.appBarTitle,
    @required this.chatListSize,
    @required this.onAddClicked,
    @required this.onThemeFlagChange,
    @required this.onLockSettingsChanged,
  });

  @override
  bool operator ==(o) =>
      o is HomeScreenViewModel &&
      isLoading == o.isLoading &&
      isLockEnabled == o.isLockEnabled &&
      isDarkTheme == o.isDarkTheme &&
      appBarTitle == o.appBarTitle &&
      chatListSize == o.chatListSize &&
      onLockSettingsChanged == o.onLockSettingsChanged &&
      onThemeFlagChange == o.onThemeFlagChange &&
      onAddClicked == o.onAddClicked;

  @override
  int get hashCode => hash4(isLoading.hashCode, isLockEnabled.hashCode, appBarTitle.hashCode, chatListSize.hashCode);
}

void changeBrightness(BuildContext context) {
  DynamicTheme.of(context)
      .setBrightness(Theme.of(context).brightness == Brightness.dark ? Brightness.light : Brightness.dark);
}

ThemeData _themeData(Brightness brightness) => _isBright(brightness) ? ThemeData.light() : ThemeData.dark();

bool _isBright(Brightness brightness) => brightness == Brightness.light;

void changeColor(BuildContext context) {
  var isBright = _isBright(DynamicTheme.of(context).brightness);
  DynamicTheme.of(context).setThemeData(
    _themeData(DynamicTheme.of(context).brightness).copyWith(
      brightness: DynamicTheme.of(context).brightness,
      accentColor: isBright ? Colors.lightBlue[400] : Colors.blueGrey[400],
      dividerColor: isBright ? Colors.lightBlue[800] : Colors.blueGrey[800],
      buttonColor: isBright ? Colors.lightBlue[400] : Colors.blueGrey[400],
      selectedRowColor: isBright ? Colors.lightBlue[400] : Colors.blueGrey[400],
      textSelectionColor: isBright ? Colors.lightBlue[700] : Colors.blueGrey[700],
      textSelectionHandleColor: isBright ? Colors.lightBlue[700] : Colors.blueGrey[700],
      highlightColor: isBright ? Colors.lightBlue[600].withOpacity(0.2) : Colors.blueGrey[600].withOpacity(0.2),
      toggleableActiveColor: isBright ? Colors.lightBlue[600] : Colors.blueGrey[600],
      cardColor: isBright ? Colors.lightBlue[100] : Colors.blueGrey[100],
    ),
  );
}

HomeScreenViewModel createHomeViewModel(Store<AppState> store) => HomeScreenViewModel._(
      isLoading: loadingStatusSelector(store.state),
      isLockEnabled: isLockEnabledSelector(store.state),
      isDarkTheme: themeFlagSelector(store.state),
      chatListSize: chatsCountSelector(store.state),
      appBarTitle: homeTitleSelector(store.state),
      onAddClicked: (context) {
        if (!loadingStatusSelector(store.state)) {
          // Version with auto generation
//          store.dispatch(AddClickedAction());

          // Version with dialog
//          showOwnDialog(
//              context: context,
//              builder: (BuildContext context) => ChatCreationDialog());

          // Version with full screen
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (context) => ChatCreationUsersScreen(),
              fullscreenDialog: true,
            ),
          );
        }
      },
      onThemeFlagChange: (isEnabled, context) {
        store.dispatch(ThemeChangesAction(isEnabled));
        changeBrightness(context);
        changeColor(context);
      },
      onLockSettingsChanged: (isEnabled) => store.dispatch(
            LockChangesAction(
              Lock(
                isEnabled: isEnabled,
                lockState: lockStateSelector(store.state),
              ),
            ),
          ),
    );
