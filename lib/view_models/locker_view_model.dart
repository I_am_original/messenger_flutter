import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:redux/redux.dart';

/// LockState listener implements Locking logic

class LockerViewModel {
  LockerViewModel._(
      Store<AppState> store, BuildContext context, bool isTriggered) {
    if (isTriggered) {
      store.dispatch(
        LockChangesAction(
          Lock(
            isEnabled: isLockEnabledSelector(store.state),
            lockState: LockState.SHOWN,
          ),
        ),
      );
      Navigator.of(context).pushNamed(routeLockScreen);
    }
  }

  @override
  bool operator ==(other) => true;

  @override
  int get hashCode => 1;
}

LockerViewModel createLockerViewModel(
        Store<AppState> store, BuildContext context) =>
    LockerViewModel._(store, context, isLockTriggeredSelector(store.state));
