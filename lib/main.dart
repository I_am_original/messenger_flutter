import 'dart:async';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/database/db_connector.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/epics.dart';
import 'package:flutter_messenger/redux/reducers.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/widgets/screens/camera_screen.dart';
import 'package:flutter_messenger/widgets/screens/chat_screen.dart';
import 'package:flutter_messenger/widgets/screens/gallery_screen.dart';
import 'package:flutter_messenger/widgets/screens/home_screen.dart';
import 'package:flutter_messenger/widgets/screens/lock_screen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

final Logger logger = new Logger("MessengerApplication");
StreamSubscription startSubscription;

void main() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${DateFormat(timeFormat).format(rec.time)}: ${rec.message}');
  });

  var store = Store<AppState>(
    messengerReducers,
    initialState: AppState.initial(),
    middleware: [
//      LoggingMiddleware.printer(),
      EpicMiddleware<AppState>(messengerEpics),
    ],
  );

  /// Read Lock status and start app on its result
  startSubscription = DatabaseConnector()
      .openDB()
      .firstWhere((db) => db != null && db.isOpen)
      .asObservable()
      .flatMap((db) => DatabaseConnector().getLock())
      .listen(
    (lock) {
      runApp(
        MessengerApplication(
          appStore: store,
          isLocked: lock != null && lock.lockState == LockState.TRIGGERED,
        ),
      );
      store.dispatch(DatabaseOpenedAction());
      store.dispatch(LockChangesAction(lock ?? Lock(isEnabled: true, lockState: LockState.IDLE)));
    },
    onDone: (() => startSubscription.cancel()),
  );
}

class MessengerApplication extends StatelessWidget {
  final Store<AppState> appStore;
  final bool isLocked;

  MessengerApplication({
    Key key,
    @required this.appStore,
    this.isLocked = false,
  }) : super(key: key);

  ThemeData themeData(Brightness brightness) => isBright(brightness) ? ThemeData.light() : ThemeData.dark();

  bool isBright(Brightness brightness) => brightness == Brightness.light;

  // This widget is the root of application and Store holder
  @override
  Widget build(BuildContext context) => StoreProvider<AppState>(
        store: appStore,
        child: DynamicTheme(
          defaultBrightness: Brightness.light,
          data: (brightness) {
            var bright = isBright(brightness);
            return themeData(brightness).copyWith(
              brightness: brightness,
              accentColor: bright ? Colors.lightBlue[400] : Colors.blueGrey[400],
              dividerColor: bright ? Colors.lightBlue[800] : Colors.blueGrey[800],
              buttonColor: bright ? Colors.lightBlue[400] : Colors.blueGrey[400],
              selectedRowColor: bright ? Colors.lightBlue[400] : Colors.blueGrey[400],
              textSelectionColor: bright ? Colors.lightBlue[700] : Colors.blueGrey[700],
              textSelectionHandleColor: bright ? Colors.lightBlue[700] : Colors.blueGrey[700],
              highlightColor: bright ? Colors.lightBlue[600].withOpacity(0.2) : Colors.blueGrey[600].withOpacity(0.2),
              toggleableActiveColor: bright ? Colors.lightBlue[600] : Colors.blueGrey[600],
              cardColor: bright ? Colors.lightBlue[100] : Colors.blueGrey[100],
            );
          },
          themedWidgetBuilder: (context, theme) {
            return MaterialApp(
              title: appTitle,
              theme: theme,
              home: isLocked ? LockScreen() : HomeScreen(),
              routes: <String, WidgetBuilder>{
                routeLockScreen: (BuildContext context) => LockScreen(),
                routeHomeScreen: (BuildContext context) => HomeScreen(),
                routeChatScreen: (BuildContext context) => ChatScreen(),
                routeCameraScreen: (BuildContext context) => CameraScreen(),
                routeGalleryScreen: (BuildContext context) => GalleryScreen(),
              },
            );
          },
        ),
      );
}
