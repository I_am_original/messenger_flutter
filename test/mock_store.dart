import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/epics.dart';
import 'package:flutter_messenger/redux/reducers.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

Store<AppState> get mockInitialStore => Store<AppState>(
      messengerReducers,
      initialState: AppState.initial(),
      middleware: [EpicMiddleware<AppState>(messengerEpics)],
    );

Store<AppState> get mockStore => Store<AppState>(
      messengerReducers,
      initialState: mockState,
      middleware: [EpicMiddleware<AppState>(messengerEpics)],
    );

AppState get mockState => AppState(
      lock: mockLockIdle,
      profileId: myProfileId,
      chatId: mockChatId,
      chatsMap: mockedChats,
      usersMap: mockedUsers,
      messagesMap: mockedMessages,
    );

Lock get mockLockIdle => Lock(
      isEnabled: true,
      lockState: LockState.IDLE,
    );

Lock get mockLockTriggered => Lock(
      isEnabled: true,
      lockState: LockState.TRIGGERED,
    );

String get mockChatId => "chatUuid1";

Map<String, Chat> get mockedChats => {
      "chatUuid1": Chat((builder) => builder
        ..id = "chatUuid1"
        ..theme = "chat1"
        ..description = "some text"
        ..userIds.replace(["userUuid1", "userUuid2"])),
      "chatUuid2": Chat((builder) => builder
        ..id = "chatUuid2"
        ..theme = "chat2"
        ..description = "some text"
        ..userIds.replace(["userUuid1", "userUuid2"])),
      "chatUuid3": Chat((builder) => builder
        ..id = "chatUuid3"
        ..theme = "chat3"
        ..description = "some text"
        ..userIds.replace(["userUuid1", "userUuid2"])),
    };

Map<String, User> get mockedUsers => {
      "userUuid1": User((builder) => builder
        ..id = "userUuid1"
        ..firstName = "user1"
        ..secondName = "111"),
      "userUuid2": User((builder) => builder
        ..id = "userUuid2"
        ..firstName = "user2"
        ..secondName = "222"),
      "30.08.1989": User((builder) => builder
        ..id = "30.08.1989"
        ..firstName = "Vadym"
        ..secondName = "Pinchuk"),
    };

Map<String, Message> get mockedMessages => {
      "messageUuid1": Message((builder) => builder
        ..id = "messageUuid1"
        ..text = "First message"
        ..userId = "userUuid1"
        ..chatId = "chatUuid2"
        ..timestamp = "2018-09-02 16:11:53.167949"),
      "messageUuid2": Message((builder) => builder
        ..id = "messageUuid2"
        ..text = "Second message"
        ..userId = "userUuid2"
        ..chatId = "chatUuid1"
        ..timestamp = "2018-08-02 16:11:53.167949"),
      "messageUuid3": Message((builder) => builder
        ..id = "messageUuid3"
        ..text = "Third message"
        ..userId = "userUuid2"
        ..chatId = "chatUuid1"
        ..timestamp = "2018-09-02 16:11:54.167949"),
      "messageUuid4": Message((builder) => builder
        ..id = "messageUuid4"
        ..text = "Fourth message"
        ..userId = "userUuid1"
        ..chatId = "chatUuid1"
        ..timestamp = "2018-09-02 16:15:53.167949"),
      "messageUuid5": Message((builder) => builder
        ..id = "messageUuid5"
        ..text = "Fifth message"
        ..userId = "userUuid2"
        ..chatId = "chatUuid1"
        ..timestamp = "2018-09-03 16:01:53.167949"),
    };
