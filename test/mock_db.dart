import 'dart:async';

import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:mockito/mockito.dart';
import 'package:sqflite/sqflite.dart';

/// Mocked Database - only used methods are implemented

class MockDatabase implements Mock, Database {
  final String _path;

  // mock of DB storage
  final Map<String, dynamic> lockMap = {};
  final Map<String, dynamic> chatsMap = {};
  final List<Map<String, dynamic>> userIdsFromChat = [];
  final Map<String, dynamic> usersMap = {};
  final Map<String, dynamic> messagesMap = {};

  MockDatabase(this._path);

  @override
  String get path => _path;

  @override
  Batch batch() => MockBatch(this);

  @override
  Future close() => Future.value(0);

  @override
  Future<int> delete(String table, {String where, List whereArgs}) {
    // TODO: implement delete
  }

  @override
  Future execute(String sql, [List arguments]) {
    // TODO: implement execute
  }

  @override
  Future<int> getVersion() => Future.value(1);

  @override
  Future<int> insert(String table, Map<String, dynamic> values,
      {String nullColumnHack, ConflictAlgorithm conflictAlgorithm}) {
    switch (table) {
      case "lock_table":
        lockMap.putIfAbsent("single", () => Lock.fromMap(values));
        break;
      case "user_table":
        User user = User.fromMap(values);
        usersMap.putIfAbsent(user.id, () => user);
        break;
      case "message_table":
        Message message = Message.fromMap(values);
        messagesMap.putIfAbsent(message.id, () => message);
        break;
    }
    return Future.value(0);
  }

  @override
  bool get isOpen => path != null;

  @override
  Future<List<Map<String, dynamic>>> query(String table,
      {bool distinct,
      List<String> columns,
      String where,
      List whereArgs,
      String groupBy,
      String having,
      String orderBy,
      int limit,
      int offset}) {
    List<dynamic> values = [];
    List<Map<String, dynamic>> valuesMaps = [];
    switch (table) {
      case "lock_table":
        values = lockMap.isNotEmpty ? lockMap.values.toList() : [];
        break;
      case "user_table":
        values = usersMap.isNotEmpty
            ? (whereArgs != null
                ? [usersMap[whereArgs[0]]]
                : usersMap.values.toList())
            : [];
        break;
      case "message_table":
        values = messagesMap.isNotEmpty ? messagesMap.values.toList() : [];
        break;
      case "chat_table":
        values = chatsMap.isNotEmpty ? chatsMap.values.toList() : [];
        break;
      case "chat_id_user_id_table":
        valuesMaps = userIdsFromChat.isNotEmpty ? userIdsFromChat : [];
        break;
    }
    values.forEach((val) => valuesMaps.add(val.toMap()));
    return Future.value(valuesMaps);
  }

  @override
  Future<int> update(String table, Map<String, dynamic> values,
      {String where, List whereArgs, ConflictAlgorithm conflictAlgorithm}) {
    switch (table) {
      case "chat_table":
        Chat chat = Chat.fromMap(values, userIdsFromChat);
        chatsMap.addAll({chat.id: chat});
        break;
    }
    return Future.value(1);
  }

  @override
  Future<List> applyBatch(Batch batch, {bool noResult}) {
    // No need to implement
    return Future.value([0]);
  }

  @override
  Future<T> devInvokeMethod<T>(String method, [arguments]) {
    // No need to implement
    return Future.value();
  }

  @override
  Future<T> devInvokeSqlMethod<T>(String method, String sql, [List arguments]) {
    // No need to implement
    return Future.value();
  }

  @override
  Future<int> rawDelete(String sql, [List arguments]) {
    // No need to implement
    return Future.value(0);
  }

  @override
  Future<int> rawInsert(String sql, [List arguments]) {
    // No need to implement
    return Future.value(0);
  }

  @override
  Future<List<Map<String, dynamic>>> rawQuery(String sql, [List arguments]) {
    // No need to implement
    return Future.value([]);
  }

  @override
  Future<int> rawUpdate(String sql, [List arguments]) {
    // No need to implement
    return Future.value(0);
  }

  @override
  Future setVersion(int version) {
    // No need to implement
    return Future.value();
  }

  @override
  Future<T> transaction<T>(Future<T> Function(Transaction txn) action,
      {bool exclusive}) {
    // No need to implement
    return Future.value();
  }
}

/// Mocked Batch - only used methods are implemented

class MockBatch implements Mock, Batch {
  final MockDatabase db;

  MockBatch(this.db);

  @override
  void insert(String table, Map<String, dynamic> values,
      {String nullColumnHack, ConflictAlgorithm conflictAlgorithm}) {
    switch (table) {
      case "chat_id_user_id_table":
        db.userIdsFromChat.add(values);
        break;
      case "chat_table":
        Chat chat = Chat.fromMap(values, db.userIdsFromChat);
        db.chatsMap.putIfAbsent(chat.id, () => chat);
        break;
    }
  }

  @override
  void update(String table, Map<String, dynamic> values,
      {String where, List whereArgs, ConflictAlgorithm conflictAlgorithm}) {
    if (table == "chat_id_user_id_table") {
      // 0 element because for test - one value is enough
      db.userIdsFromChat.clear();
      db.userIdsFromChat.add(values);
    }
  }

  @override
  Future<List> apply({bool exclusive, bool noResult}) {
    // No need to implement - DEPRECATED
    return Future.value([0]);
  }

  @override
  Future<List> commit({bool exclusive, bool noResult}) {
    // No need to implement - All actions executed on fly (Batch not Batch)
    return Future.value([0]);
  }

  @override
  void delete(String table, {String where, List whereArgs}) {
    // No need to implement
  }

  @override
  void execute(String sql, [List arguments]) {
    // No need to implement
  }

  @override
  void query(String table,
      {bool distinct,
      List<String> columns,
      String where,
      List whereArgs,
      String groupBy,
      String having,
      String orderBy,
      int limit,
      int offset}) {
    // No need to implement
  }

  @override
  void rawDelete(String sql, [List arguments]) {
    // No need to implement
  }

  @override
  void rawInsert(String sql, [List arguments]) {
    // No need to implement
  }

  @override
  void rawQuery(String sql, [List arguments]) {
    // No need to implement
  }

  @override
  void rawUpdate(String sql, [List arguments]) {
    // No need to implement
  }
}
