import 'package:flutter/material.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/main.dart';
import 'package:flutter_messenger/redux/epics.dart';
import 'package:flutter_messenger/redux/reducers.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:flutter_messenger/widgets/list_items/chat_item_widget.dart';
import 'package:flutter_messenger/widgets/list_items/message_item_widget.dart';
import 'package:flutter_messenger/widgets/loading_indicator_widget.dart';
import 'package:flutter_messenger/widgets/screens/chat_screen.dart';
import 'package:flutter_messenger/widgets/screens/home_screen.dart';
import 'package:flutter_messenger/widgets/screens/lock_screen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

import 'mock_store.dart';

void main() {
  testWidgets('1 Initial state. Application Widgets',
      (WidgetTester tester) async {
    await tester.pumpWidget(MessengerApplication(appStore: mockInitialStore));

    expect(find.text(appTitle), findsOneWidget);
    expect(find.text(emptyChatListText), findsOneWidget);
    expect(find.byType(ListView), findsNothing);
    expect(find.byIcon(Icons.add), findsOneWidget);
  });

  testWidgets('2 Initial state. Application Widgets',
      (WidgetTester tester) async {
    final widget = StoreProvider<AppState>(
        store: mockInitialStore,
        child: MaterialApp(
          home: HomeScreen(),
        ));
    await tester.pumpWidget(widget);

    expect(find.text(appTitle), findsOneWidget);
    expect(find.text(emptyChatListText), findsOneWidget);
    expect(find.byType(ListView), findsNothing);
    expect(find.byIcon(Icons.add), findsOneWidget);
  });

  testWidgets('3 Mocked state. Application Widgets',
      (WidgetTester tester) async {
    await tester.pumpWidget(MessengerApplication(appStore: mockStore));
    var size = mockStore.state.chatsMap.length;

    expect(find.text('Messenger Chats: $size'), findsOneWidget);
    expect(find.text(emptyChatListText), findsNothing);
    expect(find.byType(ListView), findsOneWidget);
    expect(find.byType(ChatItemWidget), findsNWidgets(size));
    expect(find.byIcon(Icons.add), findsOneWidget);
  });

  testWidgets('4 Mocked state. Open chat with messages',
      (WidgetTester tester) async {
    final widget = StoreProvider<AppState>(
        store: mockStore,
        child: MaterialApp(
          home: ChatScreen(),
        ));
    await tester.pumpWidget(widget);

    // Expect to see 4 messages
    expect(find.byType(MessageItemWidget), findsNWidgets(4));
  });

  testWidgets('5 Mocked state. TextField receive some input',
      (WidgetTester tester) async {
    final widget = StoreProvider<AppState>(
        store: mockStore,
        child: MaterialApp(
          home: ChatScreen(),
        ));
    await tester.pumpWidget(widget);
    await tester.enterText(find.byType(TextField), "New Message");
    await tester.pump();

    expect(find.text("New Message"), findsOneWidget);
  });

  group("Loading on", () {
    var loadingStore;
    var loadingState;
    setUp(() {
      loadingState = AppState(
        isLoading: true,
        lock: mockLockIdle,
        chatId: mockChatId,
        profileId: myProfileId,
        chatsMap: mockedChats,
        usersMap: {},
        messagesMap: {},
      );
      loadingStore = Store<AppState>(
        messengerReducers,
        initialState: loadingState,
        middleware: [EpicMiddleware<AppState>(messengerEpics)],
      );
    });

    testWidgets('6 Mocked loading state. Chat screen have loading indicator',
        (WidgetTester tester) async {
      await tester.pumpWidget(StoreProvider<AppState>(
          store: loadingStore,
          child: MaterialApp(
            home: ChatScreen(),
          )));

      // Expect to see loader
      expect(find.byType(LoadingIndicatorWidget), findsOneWidget);
    });

    testWidgets('7 Mocked loading state. Home screen have loading indicator',
        (WidgetTester tester) async {
      await tester.pumpWidget(MessengerApplication(appStore: loadingStore));

      // Expect to see loader
      expect(find.byType(LoadingIndicatorWidget), findsOneWidget);
    });
  });

  group("Loading off", () {
    testWidgets('8 Mocked state. Chat screen have no loading indicator',
        (WidgetTester tester) async {
      await tester.pumpWidget(StoreProvider<AppState>(
          store: mockStore,
          child: MaterialApp(
            home: ChatScreen(),
          )));

      // Expect to see nothing
      expect(find.byType(LoadingIndicatorWidget), findsNothing);
    });

    testWidgets('9 Mocked state. Home screen have no loading indicator',
        (WidgetTester tester) async {
      await tester.pumpWidget(MessengerApplication(appStore: mockStore));

      // Expect to see nothing
      expect(find.byType(LoadingIndicatorWidget), findsNothing);
    });
  });

  testWidgets('10 Mocked state. Lock screen enabled',
      (WidgetTester tester) async {
    final SemanticsHandle handle = tester.ensureSemantics();
    await tester.pumpWidget(MessengerApplication(appStore: mockStore));

    // Show drawer
    await tester.tap(find.byIcon(Icons.menu));
    await tester.pumpAndSettle();

    expect(find.byType(Drawer), findsOneWidget);
    expect(find.byType(SwitchListTile), findsOneWidget);
    expect(find.text(settingsLockEnabled), findsOneWidget);
    // Check that option is enabled
    expect(
        tester.getSemanticsData(find.byType(Switch)),
        matchesSemanticsData(
          hasTapAction: true,
          hasToggledState: true,
          hasEnabledState: true,
          isToggled: true,
          isEnabled: true,
        ));
    handle.dispose();
  });

  testWidgets('11 Mocked state. Lock screen on start is a root widget',
      (WidgetTester tester) async {
    await tester.pumpWidget(MessengerApplication(
      appStore: mockStore,
      isLocked: true,
    ));

    expect(find.byType(LockScreen), findsOneWidget);
    expect(find.byType(HomeScreen), findsNothing);
  });
}
