import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/reducers.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:test/test.dart';

import 'mock_store.dart';

void main() {
  //Initialization
  var homeState = AppState.initial();

  test('1 Adding new user should change users count to one', () {
    expect(homeState.usersMap.length, 0);
    //Action
    homeState = messengerReducers(
        homeState, UserCreatedAction(mockState.usersMap.values.elementAt(0)));
    //Check
    expect(homeState.usersMap.length, 1);
  });

  test('2 Adding new chat should change chats count to one', () {
    expect(homeState.chatsMap.length, 0);
    //Action
    homeState = messengerReducers(
        homeState, ChatCreatedAction(mockState.chatsMap.values.elementAt(0)));
    //Check
    expect(homeState.chatsMap.length, 1);
  });

  test('3 Adding next chat and checking chat presence', () {
    expect(homeState.chatsMap.length, 1);
    //Action
    homeState = messengerReducers(
        homeState, ChatCreatedAction(mockState.chatsMap.values.elementAt(1)));
    //Check
    expect(homeState.chatsMap.length, 2);
    expect(homeState.chatsMap.containsKey(mockState.chatsMap.keys.elementAt(1)),
        true);
  });

  test('4 Pushing wrong Action and checking size is the same', () {
    expect(homeState.chatsMap.length, 2);
    //Action
    homeState = messengerReducers(homeState, AddClickedAction());
    //Check
    expect(homeState.chatsMap.length, 2);
  });

  test('5 Adding new message should change messages count to one', () {
    expect(homeState.messagesMap.length, 0);
    //Action
    homeState = messengerReducers(homeState,
        MessageCreatedAction(mockState.messagesMap.values.elementAt(1)));
    //Check
    expect(homeState.messagesMap.length, 1);
  });

  test('6 Opening chat should change chatId', () {
    expect(homeState.chatId.isEmpty, true);
    //Action
    homeState = messengerReducers(homeState,
        OpenChatAction(mockState.chatsMap.values.elementAt(1).id));
    //Check
    expect(homeState.chatId == "chatUuid2", true);
  });

  test('7 Opening chat should show loading and hide when finished', () {
    homeState = AppState.initial();
    expect(homeState.isLoading, false);
    //Action
    homeState = messengerReducers(homeState, ChatOpenedAction());
    //Check
    expect(homeState.isLoading, true);
    //Action
    homeState = messengerReducers(
        homeState, MessagesDownloadedAction(homeState.messagesMap));
    expect(homeState.isLoading, false);
  });

  test('8 Lock state changes check', () {
    expect(homeState.lock.lockState == LockState.IDLE, true);
    //Action
    homeState = messengerReducers(
        homeState,
        LockChangesAction(
          Lock(
            isEnabled: true,
            lockState: LockState.TRIGGERED,
          ),
        ));
    //Check
    expect(homeState.lock.lockState == LockState.TRIGGERED, true);
    //Action
    homeState = messengerReducers(
        homeState,
        LockChangesAction(
          Lock(
            isEnabled: true,
            lockState: LockState.SHOWN,
          ),
        ));
    //Check
    expect(homeState.lock.lockState == LockState.SHOWN, true);
  });

  test('9 Enable/Disable lock settings check', () {
    expect(homeState.lock.isEnabled, true);
    //Action
    homeState = messengerReducers(
        homeState,
        LockChangesAction(
          Lock(
            isEnabled: false,
            lockState: LockState.IDLE,
          ),
        ));
    //Check
    expect(homeState.lock.isEnabled, false);
  });

  test('10 Delete message should work', () {
    homeState = mockState;
    //Check
    expect(messagesCountSelector(homeState), 5);
    expect(messageIdsListSelector(homeState).contains("messageUuid5"), true);
    //Action
    homeState =
        messengerReducers(homeState, MessageDeleteAction("messageUuid5"));
    //Check
    expect(messagesCountSelector(homeState), 4);
    expect(messageIdsListSelector(homeState).contains("messageUuid5"), false);
  });
}
