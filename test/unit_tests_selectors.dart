import 'package:collection/collection.dart';
import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:test/test.dart';

import 'mock_store.dart';

void main() {
  AppState state;

  //Initialization
  setUp(() {
    state = mockState;
  });

  //Clearing
  tearDown(() {
    state = null;
  });

  //Check
  test('1 profileIdSelector test', () {
    expect(profileIdSelector(state) == state.profileId, true);
  });

  // Opened Chat
  test('2 openedChatIdSelector test', () {
    expect(openedChatIdSelector(state) == state.chatId, true);
  });

  test('3 openedChatThemeSelector test', () {
    expect(openedChatThemeSelector(state) == state.chatsMap[state.chatId].theme,
        true);
  });

  test('4 openedChatSelector test', () {
    expect(openedChatSelector(state) == state.chatsMap[state.chatId], true);
  });

  // Loading status

  test('5 loadingStatusSelector test', () {
    expect(loadingStatusSelector(state), false);
  });

  // Lock state

  test('6 isLockEnabledSelector test', () {
    expect(isLockEnabledSelector(state), true);
  });

  test('7 isLockIdleSelector test', () {
    expect(isLockIdleSelector(state), true);
  });

  test('8 isLockTriggeredSelector test', () {
    expect(isLockTriggeredSelector(state), false);
  });

  test('9 isLockShownSelector test', () {
    expect(isLockShownSelector(state), false);
  });

  // Main data

  test('10 chatsMapSelector test', () {
    expect(chatsMapSelector(state) == state.chatsMap, true);
  });

  test('11 messagesMapSelector test', () {
    expect(messagesMapSelector(state) == state.messagesMap, true);
  });

  test('12.1 allUsersMapSelector test', () {
    expect(allUsersMapSelector(state) == state.usersMap, true);
  });

  test('12.2 profileUserSelector test', () {
    expect(profileUserSelector(state) == state.usersMap[state.profileId], true);
  });

  test('12.3 otherUsersMapSelector test', () {
    Function deepEq = const DeepCollectionEquality().equals;
    expect(
        deepEq(
            otherUsersMapSelector(state),
            {}
              ..addAll(state.usersMap)
              ..removeWhere((key, value) => state.profileId == key)),
        true);
  });

  test('13 chatsListSelector test', () {
    expect(chatsListSelector(state) is List, true);
    expect(chatsListSelector(state)[0] is Chat, true);
    expect(
        chatsListSelector(state)[0] == state.chatsMap.values.toList()[0], true);
  });

  test('14 messagesListSelector test', () {
    expect(messagesListSelector(state) is List, true);
    expect(messagesListSelector(state)[0] is Message, true);
    expect(
        messagesListSelector(state)[0] == state.messagesMap.values.toList()[0],
        true);
  });

  test('15.1 allUsersListSelector test', () {
    expect(allUsersListSelector(state) is List, true);
    expect(allUsersListSelector(state)[0] is User, true);
    expect(allUsersListSelector(state)[0] == state.usersMap.values.toList()[0],
        true);
    expect(
        allUsersListSelector(state).length ==
            allUsersListSelector(state).length,
        true);
  });

  test('15.2 otherUsersListSelector test', () {
    expect(otherUsersListSelector(state) is List, true);
    expect(otherUsersListSelector(state)[0] is User, true);
    expect(
        otherUsersListSelector(state).length ==
            otherUsersMapSelector(state).length,
        true);
  });

  test('16 chatIdsListSelector test', () {
    expect(chatIdsListSelector(state) is List, true);
    expect(chatIdsListSelector(state)[0] is String, true);
    expect(
        chatIdsListSelector(state)[0] == state.chatsMap.keys.toList()[0], true);
  });

  test('17 messageIdsListSelector test', () {
    expect(messageIdsListSelector(state) is List, true);
    expect(messageIdsListSelector(state)[0] is String, true);
    expect(
        messageIdsListSelector(state)[0] == state.messagesMap.keys.toList()[0],
        true);
  });

  test('18 allUserIdsListSelector test', () {
    expect(allUserIdsListSelector(state) is List, true);
    expect(allUserIdsListSelector(state)[0] is String, true);
    expect(allUserIdsListSelector(state)[0] == state.usersMap.keys.toList()[0],
        true);
    expect(
        allUserIdsListSelector(state).length ==
            allUsersListSelector(state).length,
        true);
  });

  test('18 allUserIdsListSelector test', () {
    expect(otherUserIdsListSelector(state) is List, true);
    expect(otherUserIdsListSelector(state)[0] is String, true);
    expect(
        otherUserIdsListSelector(state).length ==
            otherUsersListSelector(state).length,
        true);
  });

  test('19 chatsCountSelector test', () {
    expect(chatsCountSelector(state) == state.chatsMap.values.toList().length,
        true);
  });

  test('20 messagesCountSelector test', () {
    expect(
        messagesCountSelector(state) ==
            state.messagesMap.values.toList().length,
        true);
  });

  test('21.1 allUsersCountSelector test', () {
    expect(
        allUsersCountSelector(state) == state.usersMap.values.toList().length,
        true);
  });

  test('21.2 otherUsersCountSelector test', () {
    expect(
        otherUsersCountSelector(state) ==
            state.usersMap.values.toList().length - 1,
        true);
  });

  test('22 homeTitleSelector test with mock state in store', () {
    var count = chatsCountSelector(state);
    expect(homeTitleSelector(state) == "Messenger Chats: $count", true);
  });

  test('23 homeTitleSelector test with initial state in Store', () {
    state = AppState.initial();
    expect(homeTitleSelector(state) == appTitle, true);
  });
}
