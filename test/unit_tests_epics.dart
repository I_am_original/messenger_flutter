import 'dart:async';

import 'package:async/async.dart';
import 'package:flutter_messenger/data_models/chat.dart';
import 'package:flutter_messenger/data_models/lock.dart';
import 'package:flutter_messenger/data_models/message.dart';
import 'package:flutter_messenger/data_models/user.dart';
import 'package:flutter_messenger/redux/actions.dart';
import 'package:flutter_messenger/redux/epics.dart';
import 'package:flutter_messenger/redux/reducers.dart';
import 'package:flutter_messenger/redux/selectors.dart';
import 'package:flutter_messenger/redux/state.dart';
import 'package:flutter_messenger/resources/strings.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:test/test.dart';

import 'mock_db.dart';
import 'mock_db_connector.dart';
import 'mock_store.dart';

void main() {
  //Initialization
  EpicStore<AppState> store;

  group('createUsers epic Tests. With Initial state. So with Empty DB', () {
    setUp(() {
      store = EpicStore(Store<AppState>(
        messengerReducers,
        middleware: [EpicMiddleware<AppState>(messengerEpics)],
        initialState: AppState.initial(),
      ));
    });

    //Clearing
    tearDown(() {
      store = null;
    });

    test(
        'PUSH based: AddActionEpic emits UserCreatedAction four times, '
        'then ChatCreatedAction, while users are empty', () {
      //Action
      expect(
          createUsers(Stream.fromIterable([UsersEmptyAction()]), store),
          //Check
          emitsInOrder([
            // same as : event is UserCreatedAction
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
            TypeMatcher<UserCreatedAction>(),
          ]));
    });

    test(
        'PULL based: AddActionEpic emits UserCreatedAction four times, '
        'then ChatCreatedAction, while users are empty', () async {
      //Action
      var stream = StreamQueue(
          createUsers(Stream.fromIterable([UsersEmptyAction()]), store));
      //Check
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
      expect(await stream.next is UserCreatedAction, true);
    });
  });

  group("Tests with mocked state", () {
    setUp(() {
      store = EpicStore(Store<AppState>(
        messengerReducers,
        middleware: [EpicMiddleware<AppState>(messengerEpics)],
        initialState: mockState,
      ));
    });

    //Clearing
    tearDown(() {
      store = null;
    });

    test(
        'MessageGenerationEpic emits ChatCreatedAction, '
        'while users are in Store', () {
      //Action
      messageGenerationEpic(
              Stream.fromIterable([
                ChatCreatedAction(chatsListSelector(store.state).elementAt(0))
              ]),
              store)
          .listen((event) {
        //Check
        expect(event, TypeMatcher<MessageCreatedAction>());
      });
    });
  });

  /// DB epics test

  group("DB epics Tests", () {
    //Initialization
    var dbPath = "test.db";
    var userId = "userUuid2";
    var messageId = "messageUuid5"; // and also last message in chat 1
    var chatId = "chatUuid1";

    MockDatabaseConnector dbConnector = MockDatabaseConnector(dbPath);
    setUp(() {
      store = EpicStore(Store<AppState>(
        messengerReducers,
        middleware: [EpicMiddleware<AppState>(messengerEpics)],
        initialState: AppState.initial(),
      ));
    });

    test('DB Open epic test', () {
      //Action
      Epic<AppState> _testOpenDb = openDb(dbConnector);
      _testOpenDb(Stream.fromIterable([AppStartedAction()]), store)
          .listen((data) {
        //Check
        expect(data is DatabaseOpenedAction, true);
      });
    });

    test('DB Lock save epic test. Lock will be rewritten', () {
      Epic<AppState> _testSaveLock = saveLockToDb(dbConnector);
      //Action
      _testSaveLock(
              Stream.fromIterable([LockChangesAction(mockState.lock)]), store)
          .listen((data) {
        //Check
        expect(dbConnector.database is MockDatabase, true);
        final MockDatabase db = dbConnector.database as MockDatabase;
        expect(db.lockMap.length == 1, true);
      });
      //Action
      _testSaveLock(
              Stream.fromIterable([
                LockChangesAction(
                  Lock(
                    isEnabled: false,
                    lockState: LockState.SHOWN,
                  ),
                )
              ]),
              store)
          .listen((data) {
        //Check
        final MockDatabase db = dbConnector.database as MockDatabase;
        expect(db.lockMap.length == 1, true);
      });
    });

    test('DB User save epic test', () {
      //Action
      Epic<AppState> _testSaveUser = saveUserToDb(dbConnector);
      _testSaveUser(
              Stream.fromIterable(
                  [UserCreatedAction(allUsersMapSelector(mockState)[userId])]),
              store)
          .listen((data) {
        //Check
        expect(data is User, true);
        expect(dbConnector.database is MockDatabase, true);
        final MockDatabase db = dbConnector.database as MockDatabase;
        expect(db.usersMap.length == 1, true);
        expect(db.usersMap.containsKey(userId), true);
      });
    });

    test('DB Message save epic test', () {
      //Action
      Epic<AppState> _testSaveMessage = saveMessageToDb(dbConnector);
      _testSaveMessage(
              Stream.fromIterable([
                MessageCreatedAction(messagesMapSelector(mockState)[messageId])
              ]),
              store)
          .listen((data) {
        //Check
        expect(data is Message, true);
        expect(dbConnector.database is MockDatabase, true);
        final MockDatabase db = dbConnector.database as MockDatabase;
        expect(db.messagesMap.length == 1, true);
        expect(db.messagesMap.containsKey(messageId), true);
      });
    });

    test('DB Chat save epic test', () {
      //Action
      Epic<AppState> _testSaveChat = saveChatToDb(dbConnector);
      _testSaveChat(
              Stream.fromIterable(
                  [ChatCreatedAction(chatsMapSelector(mockState)[chatId])]),
              store)
          .listen((data) {
        //Check
        expect(data is Chat, true);
        expect(dbConnector.database is MockDatabase, true);
        final MockDatabase db = dbConnector.database as MockDatabase;
        expect(db.chatsMap.length == 1, true);
        expect(db.chatsMap.containsKey(chatId), true);
        expect(db.userIdsFromChat.length == 2, true);
        expect(db.userIdsFromChat[1].containsValue(userId + chatId), true);
      });
    });

    group('Reading from Filled DB', () {
      test('DB Read users from filled DB test', () {
        //Action
        Epic<AppState> _testReadUsers = readUsersFromDb(dbConnector);
        _testReadUsers(Stream.fromIterable([DatabaseOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is UsersDownloadedAction, true);
          expect(
              (data as UsersDownloadedAction).usersMap[userId] ==
                  allUsersMapSelector(mockState)[userId],
              true);
        });
      });

      test('DB Read messages from filled DB test', () {
        //Action
        Epic<AppState> _testReadChatMessages =
            readChatMessagesFromDb(dbConnector);
        _testReadChatMessages(
                Stream.fromIterable([ChatOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is MessagesDownloadedAction, true);
          expect(
              (data as MessagesDownloadedAction).messagesMap[messageId] ==
                  messagesMapSelector(mockState)[messageId],
              true);
        });
      });

      test('DB Read chats from filled DB test', () {
        //Action
        Epic<AppState> _testReadChats = readChatsFromDb(dbConnector);
        _testReadChats(Stream.fromIterable([DatabaseOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is ChatsDownloadedAction, true);
          expect(
              (data as ChatsDownloadedAction).chatsMap[chatId] ==
                  chatsMapSelector(mockState)[chatId],
              true);
        });
      });
    });

    group('Reading from empty DB', () {
      //Configuration
      setUp(() {
        dbConnector = MockDatabaseConnector(dbPath);
      });

      test('DB Read users from empty DB test', () {
        //Action
        Epic<AppState> _testReadUsers = readUsersFromDb(dbConnector);
        _testReadUsers(Stream.fromIterable([DatabaseOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is EmptyPayloadAction, true);
        });
      });

      test('DB Read chats from empty DB test', () {
        //Action
        Epic<AppState> _testReadChats = readChatsFromDb(dbConnector);
        _testReadChats(Stream.fromIterable([DatabaseOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is EmptyPayloadAction, true);
        });
      });

      test('DB Read last messages from empty DB test', () {
        //Action
        Epic<AppState> _testReadLastMessages =
            readLastChatsMessagesFromDb(dbConnector);
        _testReadLastMessages(
                Stream.fromIterable([DatabaseOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is EmptyPayloadAction, true);
        });
      });

      test('DB Read messages of chat from empty DB test', () {
        //Action
        Epic<AppState> _testReadChatMessages =
            readChatMessagesFromDb(dbConnector);
        _testReadChatMessages(
                Stream.fromIterable([ChatOpenedAction()]), store)
            .listen((data) {
          //Check
          expect(data is EmptyPayloadAction, true);
        });
      });
    });
  });

  test(
      'LockTriggeringEpic emits LockScreenManageAction while setting is enabled',
      () {
    final EpicStore<AppState> store = EpicStore(Store<AppState>(
      messengerReducers,
      middleware: [EpicMiddleware<AppState>(messengerEpics)],
      initialState: AppState(
          isLoading: false,
          lock: Lock(isEnabled: true, lockState: LockState.IDLE),
          chatId: null,
          profileId: null,
          chatsMap: null,
          usersMap: null,
          messagesMap: null),
    ));
    //Action
    lockTriggeringEpic(
            Stream.fromIterable([LockChangesAction(lockSelector(store.state))]),
            store)
        .listen((event) {
      //Check
      expect(event, TypeMatcher<LockChangesAction>());
      expect((event as LockChangesAction).lock.lockState == LockState.TRIGGERED,
          true);
    });
  });

  test('LockTriggeringEpic not emitting while setting is disabled', () {
    final EpicStore<AppState> store = EpicStore(Store<AppState>(
      messengerReducers,
      middleware: [EpicMiddleware<AppState>(messengerEpics)],
      initialState: AppState(
          isLoading: false,
          lock: Lock(isEnabled: false, lockState: LockState.IDLE),
          chatId: null,
          profileId: null,
          chatsMap: null,
          usersMap: null,
          messagesMap: null),
    ));
    //Action
    lockTriggeringEpic(
            Stream.fromIterable([LockChangesAction(lockSelector(store.state))]),
            store)
        .listen((event) {
      //Check
      neverEmits(TypeMatcher<LockChangesAction>());
    });
  });

  test('LockTriggeringEpic not emitting while status not IDLE', () {
    final EpicStore<AppState> store = EpicStore(Store<AppState>(
      messengerReducers,
      middleware: [EpicMiddleware<AppState>(messengerEpics)],
      initialState: AppState(
          isLoading: false,
          lock: Lock(isEnabled: true, lockState: LockState.TRIGGERED),
          chatId: null,
          profileId: null,
          chatsMap: null,
          usersMap: null,
          messagesMap: null),
    ));
    //Action
    lockTriggeringEpic(
            Stream.fromIterable([LockChangesAction(lockSelector(store.state))]),
            store)
        .listen((event) {
      //Check
      neverEmits(TypeMatcher<LockChangesAction>());
    });
  });
}
