import 'package:flutter_messenger/database/db_interface.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/src/observable.dart';
import 'package:sqflite/sqflite.dart';

import 'mock_db.dart';

class MockDatabaseConnector extends ConnectorInterface implements Mock {
  @override
  Database database;

  MockDatabaseConnector(String path) : database = MockDatabase(path);

  @override
  Observable<Database> openDB() => Observable.just(database);

  @override
  Observable closeDB() => Observable.just(0);

  @override
  Observable deleteDB() => Observable.just(0);
}
